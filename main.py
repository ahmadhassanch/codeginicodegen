
import time
from CodeGenerator.writePythonCRUD import writePythonCRUD
from CodeGenerator.writeAngGeneralPage import writeAngGeneralPage
from CodeGenerator.writeCustomForms import writeCustomForm
from CodeGenerator.preProcessConfig import preProcessConfig, outputCompactConfig
# from CodeGenerator.projects.sampleProject import modules
# from CodeGenerator.projects.sampleProject import modules
# from CodeGenerator.projects.store import modules
from CodeGenerator.projects.inventory import modules

pythonPath = "GENERATED_CODE/Accounts/"
angPath = "GENERATED_CODE/Accounts_Frontend/projects/charms-dev/src/app/"
# pythonPath = "/home/ahmad/work/SARFRAZ/Accounts/"   
# angPath = "/home/ahmad/work/SARFRAZ/Accounts_Frontend/projects/charms-dev/src/app/"

    
for module in modules:
    tableDict = preProcessConfig(module)

# for key in tableDict.keys():
# 	print(key)

# for module in modules:
#     st = outputCompactConfig(module)
# f = open("out.py", "w")
# print(st, file = f)

# print(tableDict.keys())
# exit()

for module in modules:
    print("\n===========================<< "+ module["name"].upper()+" >> ============================= ")
    time.sleep(.5)

    writePythonCRUD(pythonPath, module, tableDict)
    
    writeAngGeneralPage(angPath, module)

    pagePath = angPath + "pages/" + module["name"] +"/"
    writeCustomForm(pagePath, module["tables"], tableDict)

print("\n");
