rowActions = [
    # {"icon": "visibility", "toolTip": 'View Details', "action": 'OnViewDetails', "class": 'primary-fg'
    #     , "permission_action": 'view'},
    # {"icon": 'edit', "toolTip": 'Edit', "action": 'OnEdit', "class": 'primary-fg'},
    # {"icon": 'delete', "toolTip": 'Delete', "action": 'OnDelete', "class": 'warn-fg'}
    {'icon': 'edit', 'toolTip': 'Edit', 'action': 'OnEdit', 'class': 'primary-fg'},
    {'icon': 'delete', 'toolTip': 'Delete', 'action': 'OnDelete', 'class': 'warn-fg'}
]

manufacturerTable = {
    "title_s": "Manufacturer",
    "title_p": "Manufacturers",
    "rowActions": rowActions,
    "columns": [
        {
            "name": "manufacturer_id",
            "ctype": "Integer",
            "props": ["primary_key", "autoincrement", "required", "invisible"]
        },
        {
            "name": "manufacturer_name",
            "title": "Type Name",
            "ctype": "String(128)",
            "props": ["required", "unique", "showInSearch"]
        },
        {
            "name": "description",
            "title": "Description",
            "ctype": "String(1024)"
        },
    ]
}

unitTable = {
    "title_s": "Unit",
    "title_p": "Units",
    "rowActions": rowActions,
    "columns": [
        {
            "name": "unit_id",
            "ctype": "Integer",
            "props": ["primary_key", "autoincrement", "required", "invisible"]
        },
        {
            "name": "unit_name",
            "ctype": "String(128)",
            "title": "Name",
            "props": ["required", "unique", "showInSearch"]
        },
        {
            "name": "description",
            "ctype": "String(1024)",
            "title": "Description"
        },
        {
            "name": "is_default",
            "ctype": "Boolean",
            "title": "Default"
        },
    ]
}

formulaTable = {
    "title_s": "Formula",
    "title_p": "Formulas",
    "rowActions": rowActions,
    "columns": [
        {
            "name": "formula_id",
            "ctype": "Integer",
            "props": ["primary_key", "autoincrement", "required", "invisible"]
        },
        {
            "name": "formula_name",
            "ctype": "String(128)",
            "title": "Name",
            "props": ["required", "unique", "showInSearch"]
        },
        {
            "name": "description",
            "ctype": "String(1024)",
            "title": "Description"
        }
    ]
}

labelTable = {
    "title_s": "Label",
    "title_p": "Labels",
    "rowActions": rowActions,
    "columns": [
        {
            "name": "label_id",
            "ctype": "Integer",
            "props": ["primary_key", "autoincrement", "required", "invisible"]
        },
        {
            "name": "label_name",
            "ctype": "String(128)",
            "title": "Name",
            "props": ["required", "unique", "showInSearch"]
        },
        {
            "name": "description",
            "ctype": "String(1024)",
            "title": "Description"
        },
        {
            "name": "is_default",
            "ctype": "Boolean",
            "title": "Default"
        },
    ]
}

supplierTable = {
    "title_s": "Supplier",
    "title_p": "Suppliers",
    "rowActions": rowActions,
    "columns": [
        {
            "name": "supplier_id",
            "ctype": "Integer",
            "props": ["primary_key", "autoincrement", "required", "invisible"]
        },
        {
            "name": "supplier_name",
            "title": "Type Name",
            "ctype": "String(128)",
            "props": ["required", "unique", "showInSearch"],
        },
        {
            "name": "description",
            "title": "Description",
            "ctype": "String(1024)",
        },
        {
            "name": "postal_address",
            "title": "Postal Address",
            "ctype": "String(255)",
        },
        {
            "name": "strn",
            "title": "STRN",
            "ctype": "String(128)",
        },
        {
            "name": "ntn",
            "title": "NTN",
            "ctype": "String(128)",
        },
        {
            "name": "contact_no",
            "title": "Contact No.",
            "ctype": "String(128)",
            "props": ["required", "unique", "showInSearch"]
        },
        {
            "name": "email",
            "title": "Email",
            "ctype": "String(128)",
        },
        {
            "name": "cnic",
            "title": "CNIC",
            "ctype": "String(128)",
        },
    ]
}

inventoryTable = {
    "title_s": "Inventory",
    "title_p": "Inventories",
    "rowActions": rowActions,
    "customForm": [
        {
            'type': 'many2many',
            'n2nTable': "Inventory Formula",
            'otherTable': "formulas",
            'n2nColumn': "formula_id",
            'otherTableChipColumns': ['formula_name'],
            "allOptionsVisible": "True"

        },
        {
            'type': 'many2many',
            'n2nTable': "Inventory Label",
            'otherTable': "labels",
            'n2nColumn': "label_id",
            'otherTableChipColumns': ['label_name'],
            "allOptionsVisible": "False"

        },
    ],
    "columns": [
        {
            "name": "inventory_id",
            "ctype": "Integer",
            "props": ["primary_key", "autoincrement", "required", "invisible"]
        },
        {
            "name": "barcode",
            "title": "Bar Code",
            "ctype": "String(128)",
            "props": ["unique", "showInSearch"],
        },
        {
            "name": "inventory_name",
            "ctype": "String(128)",
            "title": "Name",
            "props": ["required", "unique", "showInSearch"]
        },
        {
            "name": "manufacturer_id",
            "title": "Manufacturer",
            "ctype": "ForeignKey",
            "addition_params": {
                "ForeignTable_s": "Manufacturer",
                "ForeignColumn": "manufacturer_id",
                "dropdownColumns": ["manufacturer_name"],
                "externalColumnDisplay": {"Manufacturer": "manufacturer_name"}
            },
        },
        {
            "name": "purchase_unit_id",
            "title": "Purchase Unit",
            "ctype": "ForeignKey",
            "props": ["required"],
            "addition_params": {
                "ForeignTable_s": "Unit",
                "ForeignColumn": "unit_id",
                "dropdownColumns": ["unit_name"],
                "externalColumnDisplay": {"Unit": "unit_name"}
            },
        },
        {
            "name": "selling_unit_id",
            "title": "Selling Unit",
            "ctype": "ForeignKey",
            "props": ["required"],
            "addition_params": {
                "ForeignTable_s": "Unit",
                "ForeignColumn": "unit_id",
                "dropdownColumns": ["unit_name"],
                "externalColumnDisplay": {"Unit": "unit_name"}
            },
        },
        {
            "name": "storage_unit_id",
            "title": "Storage Unit",
            "ctype": "ForeignKey",
            "props": ["required"],
            "addition_params": {
                "ForeignTable_s": "Unit",
                "ForeignColumn": "unit_id",
                "dropdownColumns": ["unit_name"],
                "externalColumnDisplay": {"Unit": "unit_name"}
            },
        },
        {
            "name": "purchase_to_storage",
            "title": "Purchase to Storage",
            "ctype": "Float",
            "props": ["required"],
        },
        {
            "name": "storage_to_selling",
            "title": "Storage to Selling",
            "ctype": "Float",
            "props": ["required"],
        },
        {
            "name": "is_expiry_date_enabled",
            "ctype": "Boolean",
            "booleanType": "dropdown",
            "title": "Expiry Date Enabled",
            "props": ["required"],
        },
        {
            "name": "is_narcotics",
            "ctype": "Boolean",
            "booleanType": "checkbox",
            "title": "Narcotics",
            "props": ["required"],
        },
        {
            "name": "lasa",
            "ctype": "Boolean",
            "booleanType": "checkbox",
            "title": "LASA",
            "props": ["required"],
        },
        {
            "name": "is_fridge_item",
            "ctype": "Boolean",
            "booleanType": "checkbox",
            "title": "Fridge Item",
            "props": ["required"],
        },
        {
            "name": "maximum_level",
            "title": "Maximum Level",
            "ctype": "Integer",
            "props": ["required"]
        },
        {
            "name": "minimum_level",
            "title": "Minimum Level",
            "ctype": "Integer",
            "props": ["required"]
        },
        {
            "name": "inventory_type",
            "title": "Inventory Type",
            "ctype": "String(128)"
        },
        {
            "name": "remarks",
            "title": "Remarks",
            "ctype": "String(255)"
        }
    ]
}

inventory_supplier = {
    "title_s": "Inventory Supplier",
    "title_p": "Inventory Suppliers",
    "rowActions": rowActions,
    "columns": [
        {
            "name": "id",
            "ctype": "Integer",
            "props": ["primary_key", "autoincrement", "required", "invisible"]
        },
        {
            "name": "inventory_id",
            "title": "Inventory",
            "ctype": "ForeignKey",
            "addition_params": {
                "ForeignTable_s": "Inventory",
                "ForeignColumn": "inventory_id",
                "dropdownColumns": ["inventory_name", "created_by.full_name"],
                "externalColumnDisplay": {"Inventory": "inventory_name"}
            },
        },
        {
            "name": "supplier_id",
            "title": "Supplier",
            "ctype": "ForeignKey",
            "addition_params": {
                "ForeignTable_s": "Supplier",
                "ForeignColumn": "supplier_id",
                "dropdownColumns": ["supplier_name", "created_by.full_name"],
                "externalColumnDisplay": {"Supplier": "supplier_name"}
            },
        },
    ]
}

inventory_label = {
    "title_s": "Inventory Label",
    "title_p": "Inventory Labels",
    "rowActions": rowActions,
    "columns": [
        {
            "name": "id",
            "ctype": "Integer",
            "props": ["primary_key", "autoincrement", "required", "invisible"]
        },
        {
            "name": "inventory_id",
            "title": "Inventory",
            "ctype": "ForeignKey",
            "addition_params": {
                "ForeignTable_s": "Inventory",
                "ForeignColumn": "inventory_id",
                "dropdownColumns": ["inventory_name", "created_by.full_name"],
                "externalColumnDisplay": {"Inventory": "inventory_name"}
            },
        },
        {
            "name": "label_id",
            "title": "Label",
            "ctype": "ForeignKey",
            "addition_params": {
                "ForeignTable_s": "Label",
                "ForeignColumn": "label_id",
                "dropdownColumns": ["label_name", "created_by.full_name"],
                "externalColumnDisplay": {"Label": "label_name"}
            },
        },
    ]
}

inventory_formula = {
    "title_s": "Inventory Formula",
    "title_p": "Inventory Formulas",
    "rowActions": rowActions,
    "columns": [
        {
            "name": "inventory_formula_id",
            "ctype": "Integer",
            "props": ["primary_key", "autoincrement", "required", "invisible"]
        },
        {
            "name": "inventory_id",
            "title": "Inventory",
            "ctype": "ForeignKey",
            "addition_params": {
                "ForeignTable_s": "Inventory",
                "ForeignColumn": "inventory_id",
                "dropdownColumns": ["inventory_name", "created_by.full_name"],
                "externalColumnDisplay": {"Inventory": "inventory_name"}
            },
        },
        {
            "name": "formula_id",
            "title": "Formula",
            "ctype": "ForeignKey",
            "addition_params": {
                "ForeignTable_s": "Formula",
                "ForeignColumn": "formula_id",
                "dropdownColumns": ["formula_name", "created_by.full_name"],
                "externalColumnDisplay": {"Formula": "formula_name"}
            },
        },
    ]
}

formula_interactions = {
    "title_s": "Formula Interaction",
    "title_p": "Formula Interactions",
    "rowActions": rowActions,
    "columns": [
        {
            "name": "formaula_interactions_id",
            "ctype": "Integer",
            "props": ["primary_key", "autoincrement", "required", "invisible"]
        },
        {
            "name": "formula_1_id",
            "title": "Formula  1",
            "ctype": "ForeignKey",
            "addition_params": {
                "ForeignTable_s": "Formula",
                "ForeignColumn": "formula_id",
                "dropdownColumns": ["formula_name", "created_by.full_name"],
                "externalColumnDisplay": {"Formula": "formula_name"}
            },
        },
        {
            "name": "formula_2_id",
            "title": "Formula  2",
            "ctype": "ForeignKey",
            "addition_params": {
                "ForeignTable_s": "Formula",
                "ForeignColumn": "formula_id",
                "dropdownColumns": ["formula_name", "created_by.full_name"],
                "externalColumnDisplay": {"Formula": "formula_name"}
            },
        },
    ]
}

Purchase_Requisition = {
    "title_s": "Purchase Requisition",
    "title_p": "Purchase Requisitions",
    "rowActions": rowActions,
    "customForm": [
        {
            'type': 'itemForm',
            'itemTable': "Purchase Requisition Item",
            'n2nTable': "Purchase Requisition Item",
            'otherTable': "inventories",
            'n2nColumn': "inventory_id",
            'otherTableChipColumns': ['inventory_id'],
            "allOptionsVisible": "True"

        },
    ],
    "columns": [
        {
            "name": "purchase_requisition_number",
            "ctype": "Integer",
            "props": ["primary_key", "autoincrement", "required", "invisible"]
        },
        {
            "name": "status",
            "title": "Status",
            "ctype": "String(128)",
        },
        {
            "name": "remarks",
            "title": "Remarks",
            "ctype": "String(255)",
        },
    ]
}

Purchase_Requisition_Items = {
    "title_s": "Purchase Requisition Item",
    "title_p": "Purchase Requisitions Items",
    "rowActions": rowActions,
    "columns": [
        {
            "name": "purchase_requisition_item_id",
            "ctype": "Integer",
            "props": ["primary_key", "autoincrement", "required", "invisible"]
        },
        {
            "name": "purchase_requisition_id",
            "title": "Purchase_Requisition",
            "ctype": "ForeignKey",
            "addition_params": {
                "ForeignTable_s": "Purchase Requisition", "ForeignColumn": "purchase_requisition_number",
                "dropdownColumns": ["purchase_requisition_number", "created_by.full_name"],
                "externalColumnDisplay": {"Purchase_Requisition": "purchase_requisition_number"}
            },
        },
        {
            "name": "inventory_id",
            "title": "Inventory",
            "ctype": "ForeignKey",
            "addition_params": {
                "ForeignTable_s": "Inventory", "ForeignColumn": "inventory_id",
                "dropdownColumns": ["inventory_name", "created_by.full_name"],
                "externalColumnDisplay": {"Inventory": "inventory_name"}
            },
        },
        {
            "name": "quantity",
            "title": "Quantity",
            "ctype": "Integer",
            "props": ["required"]
        },
        {
            "name": "est_rate",
            "title": "Estimated_Rate",
            "ctype": "Float",
        },
        {
            "name": "est_arrival_date",
            "title": "Estimated Arrival Date",
            "ctype": "Date",
        },
        {
            "name": "approved_quantity",
            "title": "Approved Quantity",
            "ctype": "Integer",
        }
    ]
}

Purchase_Order = {
    "title_s": "Purchase Order",
    "title_p": "Purchase Orders",
    "rowActions": rowActions,
    "customForm": [
        {
            'type': 'itemForm',
            'itemTable': "Purchase Order Item",
            'n2nTable': "Purchase Order Item",
            'otherTable': "inventories",
            'n2nColumn': "inventory_id",
            'otherTableChipColumns': ['inventory_id'],
            "allOptionsVisible": "True"
        },
    ],
    "columns": [
        {
            "name": "purchase_order_number",
            "ctype": "Integer",
            "props": ["primary_key", "autoincrement", "required", "invisible"]
        },
        {
            "name": "status",
            "title": "Status",
            "ctype": "String(128)",
        },
        {
            "name": "remarks",
            "title": "Remarks",
            "ctype": "String(1024)",
        },
        {
            "name": "supplier_id",
            "title": "Supplier",
            "ctype": "ForeignKey",
            "addition_params": {
                "ForeignTable_s": "Supplier",
                "ForeignColumn": "supplier_id",
                "dropdownColumns": ["supplier_name", "created_by.full_name"],
                "externalColumnDisplay": {"Supplier": "supplier_name"}
            },
        },
        {
            "name": "supplier_quotation_number",
            "title": "Supplier Quotation Number",
            "ctype": "String(128)",
        },
        {
            "name": "supplier_quotation_date",
            "title": "Supplier Quotation Date",
            "ctype": "Date",
        },
        {
            "name": "payment_mode",
            "title": "Paymnet Mode",
            "ctype": "String(128)",
        },
        {
            "name": "document_mode",
            "title": "Document Mode",
            "ctype": "String(128)",
        },
        {
            "name": "purchase_requisition_id",
            "title": "Purchase_Requisition",
            "ctype": "ForeignKey",
            "addition_params": {
                "ForeignTable_s": "Purchase Requisition",
                "ForeignColumn": "purchase_requisition_number",
                "dropdownColumns": ["purchase_requisition_number", "created_by.full_name"],
                "externalColumnDisplay": {"Purchase_Requisition": "purchase_requisition_number"}
            },
        },

    ]
}

Purchase_Order_Items = {
    "title_s": "Purchase Order Item",
    "title_p": "Purchase Order Items",
    "rowActions": rowActions,
    "columns": [
        {
            "name": "purchase_order_item_id",
            "ctype": "Integer",
            "props": ["primary_key", "autoincrement", "required", "invisible"]
        },
        {
            "name": "purchase_order_number",
            "title": "Purchase_Order",
            "ctype": "ForeignKey",
            "addition_params": {
                "ForeignTable_s": "Purchase Order", "ForeignColumn": "purchase_order_number",
                "dropdownColumns": ["purchase_order_number", "created_by.full_name"],
                "externalColumnDisplay": {"Purchase Order": "purchase_order_number"}
            },
        },
        {
            "name": "inventory_id",
            "title": "Inventory",
            "ctype": "ForeignKey",
            "addition_params": {
                "ForeignTable_s": "Inventory", "ForeignColumn": "inventory_id",
                "dropdownColumns": ["inventory_name", "created_by.full_name"],
                "externalColumnDisplay": {"Inventory": "inventory_name"}
            },
        },
        {
            "name": "quantity",
            "title": "Quantity",
            "ctype": "Integer",
            "props": ["required"]
        },
        {
            "name": "rate",
            "title": "Rate",
            "ctype": "Float",
            "props": ["required"]
        },
        {
            "name": "discount",
            "title": "Discount",
            "ctype": "Float",
        },
        {
            "name": "discount_type",
            "title": "Discount Type",
            "ctype": "String(128)",
        },
        {
            "name": "est_arrival_date",
            "title": "Estimated Arrival Date",
            "ctype": "Date",
        },
        {
            "name": "sales_tax",
            "title": "Sales Tax",
            "ctype": "Float",
        },
        {
            "name": "sales_tax_type",
            "title": "Sales tax Type",
            "ctype": "String(128)",
        },
    ]
}

Item_Batches = {
    "title_s": "Item Batch",
    "title_p": "Item Batches",
    "rowActions": rowActions,
    "columns": [
        {
            "name": "inventory_batch_id",
            "ctype": "Integer",
            "props": ["primary_key", "autoincrement", "required", "invisible"]
        },
        {
            "name": "inventory_id",
            "title": "Inventory",
            "ctype": "ForeignKey",
            "addition_params": {
                "ForeignTable_s": "Inventory", "ForeignColumn": "inventory_id",
                "dropdownColumns": ["inventory_name", "created_by.full_name"],
                "externalColumnDisplay": {"Inventory": "inventory_name"}
            },
        },
        {
            "name": "batch_no",
            "title": "Batch No",
            "ctype": "String(128)",
        },
        {
            "name": "expire_date",
            "title": "Expire Date",
            "ctype": "Date",
        },
        {
            "name": "remarks",
            "title": "Remarks",
            "ctype": "String(1024)",
        },
        {
            "name": "selling_price",
            "title": "Selling Price",
            "ctype": "Float",
        },
    ]
}

Store = {
    "title_s": "Store",
    "title_p": "Stores",
    "rowActions": rowActions,
    "columns": [
        {
            "name": "store_id",
            "ctype": "Integer",
            "props": ["primary_key", "autoincrement", "required", "invisible"]
        },
        {
            "name": "store_name",
            "ctype": "String(128)",
            "title": "Name",
            "props": ["required", "unique", "showInSearch"]
        },
        {
            "name": "description",
            "ctype": "String(1024)",
            "title": "Description"
        },
        {
            "name": "is_default",
            "ctype": "Boolean = False",
            "title": "Default"
        },
    ]
}

GRN = {
    "title_s": "Goods Received Note",
    "title_p": "Goods Received Notes",
    "rowActions": rowActions,
    "customForm": [
        {
            'type': 'itemForm',
            'itemTable': "GRN Item",
            'n2nTable': "GRN Item",
            'otherTable': "item_batches",
            'n2nColumn': "inventory_batch_id",
            'otherTableChipColumns': ['inventory_batch_id'],
            "allOptionsVisible": "True"
        },
    ],
    "columns": [
        {
            "name": "grn_number",
            "ctype": "Integer",
            "props": ["primary_key", "autoincrement", "required", "invisible"]
        },
        {
            "name": "status",
            "title": "Status",
            "ctype": "String(128)",
        },
        {
            "name": "remarks",
            "title": "Remarks",
            "ctype": "String(1024)",
        },
        {
            "name": "supplier_id",
            "title": "Supplier",
            "ctype": "ForeignKey",
            "addition_params": {
                "ForeignTable_s": "Supplier",
                "ForeignColumn": "supplier_id",
                "dropdownColumns": ["supplier_name", "created_by.full_name"],
                "externalColumnDisplay": {"Supplier": "supplier_name"}
            },
        },
        {
            "name": "supplier_challan_number",
            "title": "Supplier Challan Number",
            "ctype": "String(128)",
        },
        {
            "name": "supplier_challan_date",
            "title": "Supplier Challan Date",
            "ctype": "Date",
        },
        {
            "name": "payment_mode",
            "title": "Payment Mode",
            "ctype": "String(128)",
        },
        {
            "name": "document_mode",
            "title": "Document Mode",
            "ctype": "String(128)",
        },
        {
            "name": "transaction_type",
            "title": "Transaction Type",
            "ctype": "String(128)",
        },
        {
            "name": "purchase_order_id",
            "title": "Purchase Order",
            "ctype": "ForeignKey",
            "addition_params": {
                "ForeignTable_s": "Purchase Order",
                "ForeignColumn": "purchase_order_number",
                "dropdownColumns": ["purchase_order_number", "created_by.full_name"],
                "externalColumnDisplay": {"Purchase Order": "purchase_order_number"}
            },
        },
        {
            "name": "store_id",
            "title": "Store Id",
            "ctype": "ForeignKey",
            "addition_params": {
                "ForeignTable_s": "Store",
                "ForeignColumn": "store_id",
                "dropdownColumns": ["store_name", "created_by.full_name"],
                "externalColumnDisplay": {"Store Name": "store_name"}
            },
        }
    ]
}

GRN_Items = {
    "title_s": "GRN Item",
    "title_p": "GRN Items",
    "rowActions": rowActions,
    "columns": [
        {
            "name": "grn_item_id",
            "ctype": "Integer",
            "props": ["primary_key", "autoincrement", "required", "invisible"]
        },
        {
            "name": "grn_number",
            "title": "GRN",
            "ctype": "ForeignKey",
            "addition_params": {
                "ForeignTable_s": "Goods Received Note", "ForeignColumn": "grn_number",
                "dropdownColumns": ["grn_number", "created_by.full_name"],
                "externalColumnDisplay": {"Goods Received Note": "grn_number"}
            },
        },
        {
            "name": "inventory_batch_id",
            "title": "Batch ID",
            "ctype": "ForeignKey",
            "addition_params": {
                "ForeignTable_s": "Item Batch", "ForeignColumn": "inventory_batch_id",
                "dropdownColumns": ["inventory_batch_id", "created_by.full_name"],
                "externalColumnDisplay": {"Batch Item": "inventory_batch_id"}
            },
        },
        {
            "name": "quantity",
            "title": "Quantity",
            "ctype": "Integer",
            "props": ["required"]
        },
        {
            "name": "bonus",
            "title": "Bonus",
            "ctype": "Integer",
            "props": ["required"]
        },
        {
            "name": "purchase_price",
            "title": "Purchase Price",
            "ctype": "Float",
            "props": ["required"]
        },
        {
            "name": "discount",
            "title": "Discount",
            "ctype": "Float",
        },
        {
            "name": "discount_type",
            "title": "Discount Type",
            "ctype": "String(128)",
        },
        {
            "name": "sales_tax",
            "title": "Sales Tax",
            "ctype": "Float",
        },
        {
            "name": "sales_tax_type",
            "title": "Sales tax Type",
            "ctype": "String(128)",
        },
        {
            "name": "loose_packing",
            "title": "Loose Packing",
            "ctype": "Boolean",
        },
    ]
}

Purchase_return = {
    "title_s": "Purchase Return",
    "title_p": "Purchase Returns",
    "rowActions": rowActions,
    "customForm": [
        {
            'type': 'itemForm',
            'itemTable': "Purchase Return Item",
            'n2nTable': "Purchase Return Item",
            'otherTable': "purchase_return_items",
            'n2nColumn': "inventory_batch_id",
            'otherTableChipColumns': ['inventory_batch_id'],
            "allOptionsVisible": "True"
        },
    ],
    "columns": [
        {
            "name": "purchase_return_number",
            "ctype": "Integer",
            "props": ["primary_key", "autoincrement", "required", "invisible"]
        },
        {
            "name": "remarks",
            "title": "Remarks",
            "ctype": "String(1024)",
        },
        {
            "name": "payment_mode",
            "title": "Paymnet Mode",
            "ctype": "String(128)",
        },
        {
            "name": "transaction_type",
            "title": "Transaction Type",
            "ctype": "String(128)",
        },
        {
            "name": "store_id",
            "title": "Store Id",
            "ctype": "ForeignKey",
            "addition_params": {
                "ForeignTable_s": "Store",
                "ForeignColumn": "store_id",
                "dropdownColumns": ["store_name", "created_by.full_name"],
                "externalColumnDisplay": {"Store Name": "store_name"}
            },
        }
    ]
}

Purchase_return_items = {
    "title_s": "Purchase Return Item",
    "title_p": "Purchase Return Items",
    "rowActions": rowActions,
    "columns": [
        {
            "name": "purchase_return_item_id",
            "ctype": "Integer",
            "props": ["primary_key", "autoincrement", "required", "invisible"]
        },
        {
            "name": "purchase_return_number",
            "title": "Purchase Return",
            "ctype": "ForeignKey",
            "addition_params": {
                "ForeignTable_s": "Purchase Return", "ForeignColumn": "purchase_return_number",
                "dropdownColumns": ["purchase_return_number", "created_by.full_name"],
                "externalColumnDisplay": {"Purchase Return": "purchase_return_number"}
            },
        },
        {
            "name": "inventory_batch_id",
            "title": "Batch ID",
            "ctype": "ForeignKey",
            "addition_params": {
                "ForeignTable_s": "Item Batch", "ForeignColumn": "inventory_batch_id",
                "dropdownColumns": ["inventory_batch_id", "created_by.full_name"],
                "externalColumnDisplay": {"Batch Item": "inventory_batch_id"}
            },
        },
        {
            "name": "quantity",
            "title": "Quantity",
            "ctype": "Integer",
            "props": ["required"]
        },
        {
            "name": "return_price",
            "title": "Return Price",
            "ctype": "Float",
            "props": ["required"]
        },
        {
            "name": "remarks",
            "title": "Remarks",
            "ctype": "String(128)",
        },
    ]
}

Inventory_Stock = {
    "title_s": "Inventory Stock",
    "title_p": "Inventory Stocks",
    "rowActions": rowActions,
    "columns": [
        {
            "name": "inventory_stock_id",
            "ctype": "Integer",
            "props": ["primary_key", "autoincrement", "required", "invisible"]
        },
        {
            "name": "inventory_batch_id",
            "title": "Batch ID",
            "ctype": "ForeignKey",
            "addition_params": {
                "ForeignTable_s": "Item Batch", "ForeignColumn": "inventory_batch_id",
                "dropdownColumns": ["inventory_batch_id", "created_by.full_name"],
                "externalColumnDisplay": {"Batch Item": "inventory_batch_id"}
            },
        },
        {
            "name": "quantity",
            "title": "Quantity",
            "ctype": "Integer",
            "props": ["required"]
        },
        {
            "name": "store_id",
            "title": "Store Id",
            "ctype": "ForeignKey",
            "addition_params": {
                "ForeignTable_s": "Store",
                "ForeignColumn": "store_id",
                "dropdownColumns": ["store_name", "created_by.full_name"],
                "externalColumnDisplay": {"Store Name": "store_name"}
            },
        },

    ]
}

Stock_Transfer = {
    "title_s": "Stock Transfer",
    "title_p": "Stock Transfers",
    "rowActions": rowActions,
    "customForm": [
        {
            'type': 'itemForm',
            'itemTable': "Stock Transfer Item",
            'n2nTable': "Stock Transfer Item",
            'otherTable': "stock_transfer_items",
            'n2nColumn': "inventory_batch_id",
            'otherTableChipColumns': ['inventory_batch_id'],
            "allOptionsVisible": "True"
        },
    ],
    "columns": [
        {
            "name": "stock_transfer_number",
            "ctype": "Integer",
            "props": ["primary_key", "autoincrement", "required", "invisible"]
        },
        {
            "name": "from_store_id",
            "title": "From Store Id",
            "ctype": "ForeignKey",
            "addition_params": {
                "ForeignTable_s": "Store",
                "ForeignColumn": "store_id",
                "dropdownColumns": ["store_name", "created_by.full_name"],
                "externalColumnDisplay": {"Store Name": "store_name"}
            },
        },
        {
            "name": "to_store_id",
            "title": "To Store Id",
            "ctype": "ForeignKey",
            "addition_params": {
                "ForeignTable_s": "Store",
                "ForeignColumn": "store_id",
                "dropdownColumns": ["store_name", "created_by.full_name"],
                "externalColumnDisplay": {"Store Name": "store_name"}
            },
        },
        {
            "name": "remarks",
            "title": "Remarks",
            "ctype": "String(1024)",
        },
    ]
}

Stock_Transfer_items = {
    "title_s": "Stock Transfer Item",
    "title_p": "Stock Transfer Items",
    "rowActions": rowActions,
    "columns": [
        {
            "name": "purchase_return_item_id",
            "ctype": "Integer",
            "props": ["primary_key", "autoincrement", "required", "invisible"]
        },
        {
            "name": "stock_transfer_number",
            "title": "Stock Transfer",
            "ctype": "ForeignKey",
            "addition_params": {
                "ForeignTable_s": "Stock Transfer", "ForeignColumn": "stock_transfer_number",
                "dropdownColumns": ["stock_transfer_number", "created_by.full_name"],
                "externalColumnDisplay": {"Stock Transfer": "stock_transfer_number"}
            },
        },
        {
            "name": "inventory_batch_id",
            "title": "Batch ID",
            "ctype": "ForeignKey",
            "addition_params": {
                "ForeignTable_s": "Item Batch", "ForeignColumn": "inventory_batch_id",
                "dropdownColumns": ["inventory_batch_id", "created_by.full_name"],
                "externalColumnDisplay": {"Batch Item": "inventory_batch_id"}
            },
        },
        {
            "name": "quantity",
            "title": "Quantity",
            "ctype": "Integer",
            "props": ["required"]
        },
    ]
}

Sales_Invoice = {
    "title_s": "Sales Invoice",
    "title_p": "Sales Invoices",
    "rowActions": rowActions,
    "customForm": [
        {
            'type': 'itemForm',
            'itemTable': "Invoice Item",
            'n2nTable': "Invoice Item",
            'otherTable': "invoice_items",
            'n2nColumn': "inventory_batch_id",
            'otherTableChipColumns': ['inventory_batch_id'],
            "allOptionsVisible": "True"
        },
    ],
    "columns": [
        {
            "name": "invoice_number",
            "ctype": "Integer",
            "props": ["primary_key", "autoincrement", "required", "invisible"]
        },
        {
            "name": "status",
            "title": "Status",
            "ctype": "String(128)",
        },
        {
            "name": "remarks",
            "title": "Remarks",
            "ctype": "String(1024)",
        },
        {
            "name": "is_return",
            "title": "Is Return",
            "ctype": "Boolean",
        },
        {
            "name": "is_credit",
            "title": "Credit",
            "ctype": "Boolean",
        },
        # SELF RELATION
        {
            "name": "return_against",
            "title": "Return Against",
            "ctype": "ForeignKey",
            "addition_params": {
                "ForeignTable_s": "Sales Invoice", "ForeignColumn": "invoice_number",
                "dropdownColumns": ["invoice_number", "created_by.full_name"],
                "externalColumnDisplay": {"Sale Invoice": "invoice_number"}
            },
        },
        {
            "name": "transaction_id",
            "title": "Transaction ID",
            "ctype": "String(128)",
        },
        {
            "name": "payment_mode",
            "title": "Payment Mode",
            "ctype": "String(128)",
        },
        {
            "name": "patient_name",
            "title": "Patient Name",
            "ctype": "String(128)",
        },
        {
            "name": "doctor_name",
            "title": "Doctor Name",
            "ctype": "String(128)",
        },
        {
            "name": "store_id",
            "title": "Store Id",
            "ctype": "ForeignKey",
            "addition_params": {
                "ForeignTable_s": "Store",
                "ForeignColumn": "store_id",
                "dropdownColumns": ["store_name", "created_by.full_name"],
                "externalColumnDisplay": {"Store Name": "store_name"}
            },
        },
    ]
}

Sales_Invoice_Items = {
    "title_s": "Invoice Item",
    "title_p": "Invoice Items",
    "rowActions": rowActions,
    "columns": [
        {
            "name": "invoice_item_id",
            "ctype": "Integer",
            "props": ["primary_key", "autoincrement", "required", "invisible"]
        },
        {
            "name": "invoice_number",
            "title": "Invoice",
            "ctype": "ForeignKey",
            "addition_params": {
                "ForeignTable_s": "Sales Invoice", "ForeignColumn": "invoice_number",
                "dropdownColumns": ["invoice_number", "created_by.full_name"],
                "externalColumnDisplay": {"Sales Invoice": "invoice_number"}
            },
        },
        {
            "name": "inventory_batch_id",
            "title": "Batch ID",
            "ctype": "ForeignKey",
            "addition_params": {
                "ForeignTable_s": "Item Batch", "ForeignColumn": "inventory_batch_id",
                "dropdownColumns": ["inventory_batch_id", "created_by.full_name"],
                "externalColumnDisplay": {"Batch Item": "inventory_batch_id"}
            },
        },
        {
            "name": "quantity",
            "title": "Quantity",
            "ctype": "Integer",
            "props": ["required"]
        },
        {
            "name": "sale_price",
            "title": "Sale Price",
            "ctype": "Float",
            "props": ["required"]
        },
        {
            "name": "discount",
            "title": "Discount",
            "ctype": "Float",
        },
        {
            "name": "discount_type",
            "title": "Discount Type",
            "ctype": "String(128)",
        }
    ]
}

storeTables = [manufacturerTable, unitTable, formulaTable, labelTable, supplierTable, inventoryTable, inventory_formula,
               inventory_supplier, inventory_label, formula_interactions, Purchase_Requisition,
               Purchase_Requisition_Items, Purchase_Order,
               Purchase_Order_Items, Item_Batches, Store, GRN, GRN_Items, Purchase_return_items, Purchase_return,
               Inventory_Stock, Stock_Transfer,
               Stock_Transfer_items, Sales_Invoice, Sales_Invoice_Items]

invModule = {
    "name": "inventoryMod",
    "prefix": "inv_",
    "tables": storeTables
}
