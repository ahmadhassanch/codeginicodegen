rowActions = [
        # {"icon": "visibility", "toolTip": 'View Details', "action": 'OnViewDetails', "class": 'primary-fg'
        #     , "permission_action": 'view'},
        # {"icon": 'edit', "toolTip": 'Edit', "action": 'OnEdit', "class": 'primary-fg'},
        # {"icon": 'delete', "toolTip": 'Delete', "action": 'OnDelete', "class": 'warn-fg'}
        { 'icon': 'edit', 'toolTip': 'Edit', 'action': 'OnEdit', 'class': 'primary-fg' },
        { 'icon': 'delete', 'toolTip': 'Delete', 'action': 'OnDelete', 'class': 'warn-fg' }
    ]

accTypeTable = {
    "title_s": "Account Type",
    "title_p": "Account Types",
    "templates": [],
    "rowActions": rowActions,
    "columns": [
        {
            "name": "account_type_id",
            "ctype": "Integer",
            "props" : ["primary_key", "autoincrement", "required", "invisible"]
        },
        {
            "name": "account_type_name",
            "title": "Type Name",
            "ctype": "String(128)",
            "props" : ["required", "unique", "showInSearch"],
        },
        {
            "name": "description",
            "title": "Description",
            "ctype": "String(1024)",
        },
        {
            "name": "my_date",
            "title": "My New Date",
            "ctype": "Date",
        },        
    ],
    "forms" : ""

}

accTable = {
    "title_s": "Account",
    "title_p": "Accounts",
    "rowActions": rowActions,
    "columns": [
        {
            "name": "account_id",
            "ctype": "Integer",
            "props" : ["primary_key", "autoincrement", "required", "invisible"]
        },
        {
            "name": "account_type_id",
            "title": "Account Type",
            "ctype": "ForeignKey",
            "addition_params": {
                "ForeignTable_s": "Account Type",
                "ForeignColumn": "account_type_id",
                "dropdownColumns": ["account_type_name", "created_by.full_name"],
                "externalColumnDisplay": {"Account Type":"account_type_name"}
            },
            "props" : ["required", "showInSearch", "showFilter"],
        },
        {
            "name": "account_name",
            "title": "Name",
            "ctype": "String(128)",
            "props" : ["required", "sticky", "sortable"]
        },
        {
            "name": "description",
            "ctype": "String(1024)",
            "title": "Description"
        },
        {
            "name": "account_balance",
            "title": "Balance",
            "ctype": "Float = 0",
            "props" : ["required"]
        },
        {
            "name": "account_address",
            "ctype": "String(128)",
            "title": "Address"
        },
        {
            "name": "is_active",
            "title": "Is Active",
            "ctype": "Boolean = True",
            "format": "bool",  
            "props" : ["required"]
        },
    ]
}

transTable = {
    "title_s": "Transaction",
    "title_p": "Transactions",
    "rowActions": rowActions,

    "columns": [
        {
            "name": "transaction_id",
            "ctype": "Integer",
            "props" : ["primary_key", "autoincrement", "required", "invisible"]
        },
        {
            "name": "from_account_id",
            "title": "From Account",
            "ctype": "ForeignKey",
            "addition_params": {
                "ForeignTable_s": "Account",
                "ForeignColumn": "account_id",
                "dropdownColumns": ['account_name', 'account_type.account_type_name'],
                "externalColumnDisplay": {"From Account Name":"account_name", "From Account Balance": "account_balance"}
            },
            "props" : ["required"],
        },
        {
            "name": "to_account_id",
            "ctype": "ForeignKey",
            "addition_params": {
                "ForeignTable_s": "Account",
                "ForeignColumn": "account_id",
                "dropdownColumns": ['account_name', 'account_type.account_type_name'],
                "externalColumnDisplay": {"To Account Name":"account_name", "To Account Balance": "account_balance"}
            },
            "title": "To Account"
        },
        {
            "name": "amount",
            "required": "True",
            "ctype": "Float=0",
            "title": "Amount",
            "props" : ["required"]
        },
        {
            "name": "description",
            "title": "Description",
            "ctype": "String(128)"
        },
    ]
}

country = {
    "title_s": "Country",
    "title_p": "Countries",
    "rowActions": rowActions,
    "columns": [
        {
            "name": "country_id",
            "ctype": "Integer",
            "props" : ["primary_key", "autoincrement", "required", "invisible"]
        },
        {
            "name": "country_name",
            "title": "Name",
            "ctype": "String(128)",
            "props" : ["required", "unique"],
        },
    ]
}


city = {
    "title_s": "City",
    "title_p": "Cities",
    "rowActions": rowActions,
    "columns": [
        {
            "name": "city_id",
            "ctype": "Integer",
            "props" : ["primary_key", "autoincrement", "required", "invisible"]
        },
        {
            "name": "city_name",
            "title": "Name",
            "ctype": "String(128)",
            "props" : ["required", "sortable", "sticky"]
         },
         {
            "name": "country_id",
            "ctype": "ForeignKey",
            "addition_params": {
                "ForeignTable_s": "Country",
                "ForeignColumn": "country_id",
                "dropdownColumns": ["country_name", "created_by.full_name"],
                "externalColumnDisplay": {"Country Name":"country_name", "Country ID":"country_id"}
            },
            "props" : ["required"],
            "title": "Country",
        },
    ]
}

accTables = [accTypeTable, accTable, transTable, country, city]

accModule = {
    "name" : "accountsMod",
    "prefix" : "acc_",
    "tables" : accTables
}


