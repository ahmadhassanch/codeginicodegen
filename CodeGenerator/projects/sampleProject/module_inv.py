rowActions = [
        # {"icon": "visibility", "toolTip": 'View Details', "action": 'OnViewDetails', "class": 'primary-fg'
        #     , "permission_action": 'view'},
        # {"icon": 'edit', "toolTip": 'Edit', "action": 'OnEdit', "class": 'primary-fg'},
        # {"icon": 'delete', "toolTip": 'Delete', "action": 'OnDelete', "class": 'warn-fg'}
        { 'icon': 'edit', 'toolTip': 'Edit', 'action': 'OnEdit', 'class': 'primary-fg' },
        { 'icon': 'delete', 'toolTip': 'Delete', 'action': 'OnDelete', 'class': 'warn-fg' }
    ]

manufacturerTable = {
    "title_s": "Manufacturer",
    "title_p": "Manufacturers",
    "rowActions": rowActions,
    "columns": [
        {
            "name": "manufacturer_id",
            "ctype": "Integer",
            "props" : ["primary_key", "autoincrement", "required", "invisible"]
        },
        {
            "name": "manufacturer_name",
            "title": "Type Name",
            "ctype": "String(128)",
            "props" : ["required", "unique", "showInSearch"],
        },
        {
            "name": "description",
            "title": "Description",
            "ctype": "String(1024)"
        },
    ]
}

unitTable= {
    "title_s": "Unit",
    "title_p": "Units",
    "rowActions": rowActions,
    "columns": [
        {
            "name": "unit_id",
            "ctype": "Integer",
            "props" : ["primary_key", "autoincrement", "required", "invisible"]
        },
         {
            "name": "unit_name",
            "ctype": "String(128)",
            "title": "Name"
        },
        {
            "name": "description",
            "ctype": "String(1024)",
            "title": "Description"
        },
        {
            "name": "is_default",
            "ctype": "Boolean",
            "title": "Default"
        },
    ]
}

formulaTable= {
    "title_s": "Formula",
    "title_p": "Formulas",
    "rowActions": rowActions,
    "columns": [
        {
            "name": "formula_id",
            "ctype": "Integer",
            "props" : ["primary_key", "autoincrement", "required", "invisible"]
        },
        {
            "name": "formula_name",
            "ctype": "String(128)",
            "title": "Name"
        },
        {
            "name": "description",
            "ctype": "String(1024)",
            "title": "Description"
        }
    ]
}

lableTable= {
    "title_s": "Label",
    "title_p": "Labels",
    "rowActions": rowActions,
    "columns": [
        {
            "name": "label_id",
            "ctype": "Integer",
            "props" : ["primary_key", "autoincrement", "required", "invisible"]
        },
         {
            "name": "label_name",
            "ctype": "String(128)",
            "title": "Name"
        },
        {
            "name": "description",
            "ctype": "String(1024)",
            "title": "Description"
        },
        {
            "name": "is_default",
            "ctype": "Boolean",
            "title": "Default"
        },
    ]
}


supplierTable = {
    "title_s": "Supplier",
    "title_p": "Suppliers",
    "rowActions": rowActions,
    "columns": [
        {
            "name": "supplier_id",
            "ctype": "Integer",
            "props" : ["primary_key", "autoincrement", "required", "invisible"]
        },
        {
            "name": "supplier_name",
            "title": "Type Name",
            "ctype": "String(128)",
            "props" : ["required", "unique", "showInSearch"],
        },
        {
            "name": "description",
            "title": "Description",
            "ctype": "String(1024)",
        },
        {
            "name": "strn",
            "title": "STRN",
            "ctype": "String(128)",
        },
        {
            "name": "ntn",
            "title": "NTN",
            "ctype": "String(128)",
        },
        {
            "name": "contact_no",
            "title": "Contact No.",
            "ctype": "String(128)",
        },
        {
            "name": "email",
            "title": "Email",
            "ctype": "String(128)",
        },
        {
            "name": "cnic",
            "title": "CNIC",
            "ctype": "String(128)",
        },
    ]
}


inventoryTable = {
    "title_s": "Inventory",
    "title_p": "Inventories",
    "rowActions": rowActions,
    "columns": [
        {
            "name": "code",
            "ctype": "Integer",
            "props" : ["primary_key", "autoincrement", "required", "invisible"]
        },
        {
            "name": "barcode",
            "title": "Bar Code",
            "ctype": "String(128)",
            "props" : [ "unique", "showInSearch"],
        },
         {
            "name": "name",
            "ctype": "String(128)",
            "title": "Name",
            "props" : ["required", "unique", "showInSearch"],  
        },
        {
            "name": "description",
            "title": "Description",
            "ctype": "String(1024)",
             
        },
        {
            "name": "manufacturer",
            "title": "Manufacturer",
            "ctype": "ForeignKey",
            "addition_params": {
                "ForeignTable_s": "Manufacturer",
                "ForeignColumn": "manufacturer_id",
                "dropdownColumns": ["manufacturer_name", "created_by.full_name"],
                "externalColumnDisplay": {"Manufacturer":"manufacturer_name"}
            },
        },
        {
            "name": "supplier",
            "title": "Supplier",
           "ctype": "ForeignKey",
            "addition_params": {
                "ForeignTable_s": "Supplier",
                "ForeignColumn": "supplier_id",
                "dropdownColumns": ["supplier_name", "created_by.full_name"],
                "externalColumnDisplay": {"Supplier":"supplier_name"}
            },
        },
        {
            "name": "purchase_unit",
            "title": "Purchase Unit",
            "ctype": "ForeignKey",
            "addition_params": {
                "ForeignTable_s": "Unit",
                "ForeignColumn": "unit_id",
                "dropdownColumns": ["unit_name", "created_by.full_name"],
                "externalColumnDisplay": {"Unit":"unit_name"}
            },
        },
        {
            "name": "storage_unit",
            "title": "Storage Unit",
            "ctype": "ForeignKey",
            "addition_params": {
                "ForeignTable_s": "Unit",
                "ForeignColumn": "unit_id",
                "dropdownColumns": ["unit_name", "created_by.full_name"],
                "externalColumnDisplay": {"Unit":"unit_name"}
            },
        },
        {
            "name": "selling_unit",
            "title": "Selling Unit",
            "ctype": "ForeignKey",
            "addition_params": {
                "ForeignTable_s": "Unit",
                "ForeignColumn": "unit_id",
                "dropdownColumns": ["unit_name", "created_by.full_name"],
                "externalColumnDisplay": {"Unit":"unit_name"}
            },
        },
        {
            "name": "purchase_to_storage",
            "title": "Purchase to Storage",
            "ctype": "Float",
            "props" : ["required"], 
            
        },
        {
            "name": "storage_to_selling",
            "title": "Storage to Selling",
            "ctype": "Float",
            "props" : ["required"], 
            
        },
        {
            "name": "expiry_date_enabled",
            "ctype": "Boolean",
            "title": "Expiry Date Enabled",
            "props" : ["required"], 
        },
        {
            "name": "narcotics",
            "ctype": "Boolean",
            "title": "Narcotics",
            "props" : ["required"], 
        },
    ]
}

invTables = [manufacturerTable, unitTable, formulaTable, lableTable, supplierTable, inventoryTable]

invModule = {
    "name" : "inventoryMod",
    "prefix" : "inv_",
    "tables" : invTables
}

