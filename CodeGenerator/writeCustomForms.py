from .utils import mkdir, findTableFromTableName

def writeDateFieldHTML(column):  #not tested yet, never occured
    st = ""
    st += '                    <mat-form-field fxFlex="30" appearance="outline">\n'
    st += '                        <mat-label>' + column['title']+ '</mat-label>\n'
    st += '                        <chi-date-picker [field]="' + camelCase(column['name']) + '"></chi-date-picker>\n'
    st += '                    </mat-form-field>\n'
    # print("==================================DATE FIELD NOT TESTED =======================");
    return st

def camelCase(name):
    v = name.split("_");
    st = v[0]
    for i, value in enumerate(v):
        if i==0 : continue
        st += v[i].title()
    return st

def writeForeignFieldHTML(column):
    # fModel = column["addition_params"]["externalModelCamel"]
    st = ""
    st += '                <mat-form-field fxFlex="30" appearance="outline">\n'
    st += '                    <mat-label>'+ column['title']+'</mat-label>\n'
    st += '                    <chi-select [field]="' + camelCase(column['name']) + '"></chi-select>\n'
    st += '                </mat-form-field>\n'
    st += '        \n'    
    return st

def writeStringFieldHTML(column):
    st = ""
    st += '                <mat-form-field fxFlex="30" appearance="outline">\n'
    st += '                    <mat-label>' + column['title'] + '</mat-label>\n'
    st += '                    <input matInput type="text" formControlName="'+ column['name']+'"/>\n'
    st += '                </mat-form-field>\n'
    st += '        \n'    
    return st;

def writeBooleanFieldDropDownHTML(column):
    st = ""
    st += '                <mat-form-field fxFlex="30" appearance="outline">\n'
    st += '                    <mat-label>' + column["title"] + '</mat-label>\n'
    st += '                    <mat-select formControlName="'+column["name"]+'">\n'
    st += '                      <mat-option [value]="true"> Yes </mat-option>\n'
    st += '                      <mat-option [value]="false"> No </mat-option>\n'
    st += '                    </mat-select>\n'
    st += '                </mat-form-field>\n'
    st += '        \n' 
    return st

def writeBooleanFieldCheckboxHTML(column):
    st = ""
    st += '                <div fxFlex="30" fxLayoutAlign="start center">\n'
    st += '                <mat-checkbox  formControlName="'+ column["name"]+'"\n'
    st += '                  >' + column["title"]+ '</mat-checkbox>\n'
    st += '                </div>\n'
    st += '        \n' 
    return st

def writeBooleanFieldHTML(column):
    if "booleanType" in column:
        if(column["booleanType"] == "checkbox"):
            st = writeBooleanFieldCheckboxHTML(column)
        else :#(column["booleanType"] == "dropdown"):
            st = writeBooleanFieldDropDownHTML(column)
    else:
        st = writeBooleanFieldDropDownHTML(column)

    return st


def writeFloatFieldHTML(column):
    st = ""
    st += '                <mat-form-field fxFlex="30" appearance="outline">\n'
    st += '                    <mat-label>' + column['title'] + '</mat-label>\n'
    st += '                    <input matInput type="number" formControlName="'+ column['name']+'"/>\n'
    st += '                </mat-form-field>\n'
    st += '        \n'    
    return st;    

def writeFormHeader(title):
    st = ""
    st +='<div fxFlex="100" fxLayout="column">\n'
    st +='    <div fxLayoutAlign="center">\n'
    st +='        <h3 fxFlex="90" class="form-header"> '+ title +'</h3>\n'
    st +='    </div>\n'
    st +='    <div fxFlex="90" fxLayoutAlign="center">\n'
    st +='        <div fxFlex="90" fxLayout="column">\n'


    st +='            <form [formGroup]="theForm" fxLayout="row wrap" fxLayoutAlign="space-between">\n'
    st += "\n"
    return st

def writeFormFooter():
    st = ""
    st += '            </form>\n'
    st += '            <div fxFlex="10" fxLayout="row">\n'
    st += '                <div fxFlex="100" fxLayoutGap="1em" fxLayoutAlign="end center">\n'
    st += '                    <button mat-raised-button (click)="onSave()" color="primary">Save</button>\n'
    st += '                    <button mat-stroked-button (click)="onClear()" color="primary">Clear</button>\n'
    st += '                </div>\n'
    st += '            </div>\n'
    st += '        </div>\n'
    st += '    </div>\n'
    st += '</div>\n'


    return st

def getColumnFromName(tableData, columnName):
    for column in tableData["columns"]:
        if column["name"] == columnName:
            return column
    print("Wrong column", columnName)
    exit()    


def writeMultiSelectFieldHTML(item):
    st = ""
    showAll = "false"
    if(item["allOptionsVisible"] == "True"):
        showAll = "true"
    st += '                <chi-n2n-select fxFlex="30" [showAll]="'+ showAll +'" [field]="'+item["otherTable"]+'"></chi-n2n-select>\n'
    st += "\n"
    return st

def writeItemFieldHTML(item):
    st = ""
    st += '                <div fxFlex="100">\n'
    st += '                    <line-item fxFlex="100" [defaultEdit]="false" [config]="lineItemConfig" [data]="lineItemData"></line-item>\n'
    st += '                </div>\n'


    return st;

def writeCustomFormHTML(tableData, codePath):
    slug = tableData["slug"]
    # formConfig = tableData["customForm"]
    st = writeFormHeader(tableData["title_s"])

    for column in tableData["columns"]:
   
        #if isinstance(item, str):
        # st += "                " + columnName + "\n"
        ##column = getColumnFromName(tableData, item)
        #print("================" + column['name'])
        if(column['ctype'][:6] == 'String'):
            st += writeStringFieldHTML(column)

        if(column['ctype'] == 'ForeignKey'):
            st += writeForeignFieldHTML(column)

        if(column['ctype'] == 'Date'):
            st += writeDateFieldHTML(column)

        if(column['ctype'] == 'Float'):
            st += writeFloatFieldHTML(column)

        if(column['ctype'] == 'Boolean'):
            st += writeBooleanFieldHTML(column)


    for item in tableData["customForm"]:
        
        if(item["type"] ==  "many2many"):
            st += writeMultiSelectFieldHTML(item)
        elif(item["type"] ==  "itemForm"):
            st += writeItemFieldHTML(item)
        else:
            print("Error while writing custom HTML, unknown type: ", item["type"]);

        
    st += writeFormFooter()

    
    f = open(codePath + slug + '.component.html', 'w')
    print(st, file=f)


def writeDateFieldTS(st1, st2, st3, st4, column):  #not tested yet, never occured
    st1 += "    " + camelCase(column['name']) + ": FormField;\n"

    #todo: fix the fields below
    st2 += "        "
    st2 += "this."+camelCase(column['name'])+" = new FormField({name: 'date', title: 'Date', type: 'date', required: false});\n"

    st2 += "        "
    st2 += "this.theForm.addControl('"+ column['name'] +"', this." + camelCase(column['name']) + ".formControl);\n\n"
    return st1, st2, st3, st4

    # this.formulaField = new FormField({
    #   name: 'formula_id',
    #   title: 'Formula Name',
    #   type: 'foreign',
    #   required: false,
    #   foreign: {
    #     foreign_table: 'formulas',
    #     foreign_column: 'formula_id',
    #     columns: ['formula_name'],
    #   },
    # });
  # {
  #                           'n2nTable': "Inventory Formula",
  #                           'otherTable': "formulas",
  #                           'n2nColumn':  "formula_id",
  #                           'otherTableChipColumns': ['formula_name']
  #                       }
    

def writeMultiSelectFieldTS(st1, st2, st3, st4, column):
    st1 += "    " + camelCase(column['otherTable']) + ": FormField;\n"

    st2 += "        this."+camelCase(column['otherTable'])+" = new FormField({\n"
    st2 += "                        name: '"+column['n2nColumn']+"',\n"
    st2 += "                        title: '"+column['otherTable'].title()+"', \n"
    st2 += "                        type: 'foreign',\n"
    if "required" in column["props"]:
        st2 += "                        required: false,\n" 
    st2 += "                        foreign: {\n"
    st2 += f"                            foreign_table: '{column['formulas']}',\n"
    st2 += f"                            foreign_column: '{column['addition_params']['ForeignColumn']}',\n"

    s1 = ""
    for s in column["addition_params"]["dropdownColumns"]:
        s1 += "'" + s + "',"

    st2 += "                            columns: ["+s1+"]\n"
    st2 += "                        }\n"
    st2 += "        });\n"
    # st3 += "///////////////////////"

    st2 += "        "
    st2 += "this.theForm.addControl('"+ column['name'] +"', this." + camelCase(column['name']) + ".formControl);\n\n"

    return st1, st2, st3, st4


def writeForeignFieldTS(st1, st2, st3, st4, column):
    st1 += "    " + camelCase(column['name']) + ": FormField;\n"

    st2 += "        this."+camelCase(column['name'])+" = new FormField({\n"
    st2 += "                        name: '"+column['name']+"',\n"
    st2 += "                        title: '"+column['title']+"', \n"
    st2 += "                        type: 'foreign',\n"
    if "required" in column["props"]:
        st2 += "                        required: true,\n" 
    st2 += "                        foreign: {\n"
    st2 += f"                            foreign_table: '{column['addition_params']['externalTable_c']}',\n"
    st2 += f"                            foreign_column: '{column['addition_params']['ForeignColumn']}',\n"

    s1 = ""
    for s in column["addition_params"]["dropdownColumns"]:
        s1 += "'" + s + "',"

    st2 += "                            columns: ["+s1+"]\n"
    st2 += "                        }\n"
    st2 += "        });\n"
    # st3 += "///////////////////////"

    st2 += "        "
    st2 += "this.theForm.addControl('"+ column['name'] +"', this." + camelCase(column['name']) + ".formControl);\n\n"

    return st1, st2, st3, st4

def writeStringFieldTS(st1, st2, st3, st4, column):
    st3 += "            "
    st3 += column["name"] + ": new FormControl(''"
    if 'required' in column["props"]:
        st3 += ", Validators.required"
    st3  += "),\n"

    return st1, st2, st3, st4

def writeFloatFieldTS(st1, st2, st3, st4, column):
    st3 += "            "
    st3 += column["name"] + ": new FormControl(''"
    if 'required' in column["props"]:
        st3 += ", Validators.required"
    st3  += "),\n"

    return st1, st2, st3, st4



def writeBooleanFieldTS(st1, st2, st3, st4, column):
    st3 += "            "
    st3 += column["name"] + ": new FormControl(false"
    if 'required' in column["props"]:
        st3 += ", Validators.required"
    st3  += "),\n"

    return st1, st2, st3, st4


    print("Table not found in findTableFromTableName", tableName)
    exit()

def writeItemFieldTS(st1, st2, st3, st4, currentTable, itemTable, configTables, tableDict):
    #st3 += "            "
    st1 += '    @ViewChildren(LineItemComponent) lineItems: QueryList<LineItemComponent>;\n'
    st1 += '    lineItemConfig: any;\n'
    st1 += '    lineItemData = [];\n'
    
    st2 += '\n'

    st2 += "        this.lineItemConfig  = {\n"
    st2 += "            name: 'line_items',\n"
    st2 += "            items: [{\n"
    st2 += "                editing: true,\n"
    st2 += "                fields: [\n"

    table = findTableFromTableName(itemTable, configTables)
    # st2 += "    " + itemTable["title_p"]
    for column in table["columns"]:
        if ('invisible' in column["props"]):
            continue

        if column["ctype"] == "ForeignKey":
            if column["addition_params"]["ForeignTable_s"] == currentTable:
                continue

        ctype = "text"
        
        if column["ctype"] == "ForeignKey":
            foreignTable = column["addition_params"]["ForeignTable_s"];
            foreignColumn = column["addition_params"]["ForeignColumn"];

            foreignTableName = tableDict[foreignTable]["tableName_c"]
            st2 += "                    {\n"
            st2 += "                        title: '" + foreignTable + "',\n"
            st2 += "                        value: null,\n"
            st2 += "                        name: '" + foreignColumn + "',\n"
            st2 += "                        displayValue: null,\n"
            st2 += "                        field: 'foreign',\n"
            st2 += "                        required: true,\n"
            st2 += "                        options: { name: '" + foreignColumn + "', title: '" + foreignTable + "', required: true, type: 'foreign',\n"
            st2 += "                            foreign: {\n"
            st2 += "                                foreign_table: '"+foreignTableName+"', foreign_column: '" + foreignColumn + "',\n"
            st2 += "                                columns: ['" + foreignColumn + "']\n"
            st2 += "                            }\n"
            st2 += "                        },\n"
            st2 += "                    },\n"

        elif column["ctype"] == "Float" or column["ctype"] == "Integer":
            ctype = "number"
            st2 += "                    {\n"
            st2 += "                        title: '"+column["title"]+"',\n"
            st2 += "                        value: null,\n"
            st2 += "                        field: 'input',\n"
            st2 += "                        name: '"+column["name"]+"',\n"
            st2 += "                        type: '"+ctype+"',\n"
            st2 += "                        required: false\n"
            st2 += "                    },\n"

        elif column["ctype"] == "Date":
            st2 += "                    {\n"
            st2 += "                        title: '"+column["title"]+"',\n"
            st2 += "                        value: null,\n"
            st2 += "                        field: 'date',\n"
            st2 += "                        name: '"+column["name"]+"',\n"
            st2 += "                        required: false\n"
            st2 += "                    },\n"

        elif column["ctype"] == "Boolean":
            st2 += "                    {\n"
            st2 += "                        title: '"+column["title"]+"',\n"
            st2 += "                        value: false,\n"
            st2 += "                        field: 'checkbox',\n"
            st2 += "                        name: '"+column["name"]+"',\n"
            st2 += "                        required: false\n"
            st2 += "                        label_hidden: true\n"
            st2 += "                    },\n"

        elif column["ctype"][:6] == "String":
            st2 += "                    {\n"
            st2 += "                        title: '"+column["title"]+"',\n"
            st2 += "                        value: null,\n"
            st2 += "                        field: 'input',\n"
            st2 += "                        name: '"+column["name"]+"',\n"
            st2 += "                        type: '"+"text"+"',\n"
            st2 += "                        required: false\n"
            st2 += "                    },\n"

        else:
            print("unknown column type while writing itemForm", column["ctype"]);

    st2 += "                ]\n"
    st2 += "           }]\n"
    st2 += "       };\n"
    st2 += "\n"

    # st3 += column["name"] + ": new FormControl(false"
    # if 'required' in column["props"]:
    #     st3 += ", Validators.required"
    # st3  += "),\n"

    return st1, st2, st3, st4


def writeMany2ManyFieldTS(st1, st2, st3, st4, item):
    # st3 += "            "
    # st3 += column["name"] + ": new FormControl(''"
    # if 'required' in column["props"]:
    #     st3 += ", Validators.required"
    # st3  += "),\n"
    st1 += "    " + item["otherTable"] + ": FormField;\n"

    st2 += "    this."+item["otherTable"]+" = new FormField({\n"
    st2 += "      name: '"+item["n2nColumn"]+"',\n"
    st2 += "      title: '"+item["otherTable"]+"',\n"
    st2 += "      type: 'foreign',\n"
    st2 += "      required: false,\n"
    st2 += "      foreign: {\n"
    st2 += "        foreign_table: '"+item["otherTable"]+"',\n"
    st2 += "        foreign_column: '"+item["n2nColumn"]+"',\n"

    sx = ""
    for v in item['otherTableChipColumns']:
        sx += "'" + v + "', "

    st2 += "        columns: ["+ sx +"],\n"
    st2 += "      },\n"
    st2 += "    });\n\n"
    st2 += "    this.theForm.addControl('"+ item["otherTable"]+"', this."+item["otherTable"]+ ".formControl);\n\n"


    return st1, st2, st3, st4


def writeCustomFormTS(tableData, codePath, configTables, tableDict):
    slug = tableData["slug"]
    # formConfig = tableData["customForm"]
    tableName = tableData["tableName_c"]
    st1 = ""
    st2 = ""
    st3 = ""
    st4 = ""

    for column in tableData["columns"]:

        #column = getColumnFromName(tableData, item)

        if(column['ctype'][:6] == 'String'):
            st1, st2, st3, st4 = writeStringFieldTS(st1, st2, st3, st4, column)

        if(column['ctype'] == 'ForeignKey'):
            st1, st2, st3, st4 = writeForeignFieldTS(st1, st2, st3, st4, column)

        if(column['ctype'] == 'Date'):
            st1, st2, st3, st4 = writeDateFieldTS(st1, st2, st3, st4, column)

        if(column['ctype'] == 'Boolean'):
            st1, st2, st3, st4 = writeBooleanFieldTS(st1, st2, st3, st4, column)

        if(column['ctype'] == 'Float'):
            st1, st2, st3, st4 = writeFloatFieldTS(st1, st2, st3, st4, column)

        # st1, st2, st3, st4 = writeForeignFieldTS(st1, st2, st3, st4, item)                    

    st1 += "\n"
    itemFlag = False
    for item in tableData["customForm"]:
        if(item["type"] ==  "many2many"):
            st1, st2, st3, st4 = writeMany2ManyFieldTS(st1, st2, st3, st4, item)
        elif(item["type"] ==  "itemForm"):
            itemFlag = True
            #st1, st2, st3, st4 = writeMany2ManyFieldTS(st1, st2, st3, st4, item)
            st1, st2, st3, st4 = writeItemFieldTS(st1, st2, st3, st4, tableData["title_s"], item["itemTable"], configTables, tableDict)

        else:
            print("Error while writing custom form, unknown type: ", item["type"])
            exit()
        # st1, st2, st3, st4 = writeMany2ManyFieldTS(st1, st2, st3, st4, item)

    st = ""

    st += "import { Component, OnInit } from '@angular/core';\n"
    st += "import { ApiService } from 'charms-lib';\n"
    st += "import { FormGroup, FormControl, Validators } from '@angular/forms';\n"
    st += "import { FormField, GenericApiResponse, RVAlertsService } from 'charms-lib';\n"

    if itemFlag == True:
        st += "import { QueryList, ViewChildren } from '@angular/core';\n"
        st += "import { LineItemComponent } from '../../shared/line-item/line-item.component';\n"


    st += "\n"
    st += "\n"
    st += "@Component({\n"
    st += "    selector: '" + slug + "-form',\n"
    st += "    templateUrl: '" + slug + ".component.html',\n"
    st += "    styleUrls: ['" + slug + ".component.scss']\n"
    st += "})\n"
    st += "export class "+ tableData['modelName']+"FormComponent implements OnInit\n"
    st += "{\n"
    st += "    theForm: FormGroup;\n"
    # st += "    accountType: FormField;\n"
    st +=  st1
    st += "    \n"
    st += "    constructor(private apiService: ApiService)\n"
    st += "    {\n"
    # st += st2
    #st += "        \n"
    #st += "        this.theForm = new FormGroup({\n"
    # st += st3
    #st += "        });\n"
    st += "    }\n"
    st += "    \n"
    st += "    ngOnInit(): void\n"
    st += "    {\n"
    st += "         this.theForm = new FormGroup({\n"
    st += st3
    st += "         });\n"
    #st += st4
    st += st2
    st += "    }\n"
    st += "    \n"

    st += "    onClear():void {\n"
    st += "      this.theForm.reset();\n"
    st += "    }\n\n"

    st += "    onSave(): void \n"
    st += "    {\n"
    st += "        const payload = this.theForm.value;\n"

    if(item["type"] ==  "itemForm"):
        st += "        for (const item of this.lineItems) {\n"
        st += "            const data = item.getData();\n"
        st += "            if (data == null) {\n"
        st += "              return;\n"
        st += "            }\n"
        st += "            payload.items = data.items;\n"
        st += "        }\n"


    st += "\n"
    st += "        this.apiService.post('"+ tableData['tableName_c'] +"/Create', payload).then((resp: GenericApiResponse) => {\n"
    st += "            RVAlertsService.success('Success', '"+ tableData['title_s'] +" Created').subscribe(resp => {\n"
    st += "                this.theForm.reset();              \n"
    st += "            })\n"
    st += "        }, (error: GenericApiResponse) => {\n"
    st += "            RVAlertsService.error('Error Creating "+ tableData['title_s'] +"', error.ErrorMessage);\n"
    st += "        })\n"
    st += "    }\n"
    st += "    \n"
    st += "}\n"
    
    f = open(codePath + slug + '.component.ts', 'w')
    print(st, file=f)


def writeCustomFormSCSS(tableData, codePath):
    slug = tableData["slug"]
    st = ""

    st += ".container\n"
    st += "{\n"
    st += "    padding: 10px;\n"
    st += "}    \n"

    f = open(codePath + slug + '.component.scss', 'w')
    print(st, file=f)

def writeCustomFormModule(tableData, codePath):
    slug = tableData["slug"]
    model = tableData["modelName"]

    includes1 = {};
    for column in tableData["columns"]:

        if(column['ctype'] == 'ForeignKey'):
            includes1["SelectModule"] = ""

        if(column['ctype'] == 'Date'):
            includes1["DatePickerModule"] = ""

    sx1 = ""
    sx2 = ""
    for key in includes1:
        sx1 += ", " + key
        sx2 += "        " + key + ",\n"

    st = ""
    st += "import { NgModule } from '@angular/core';\n"
    st += "import { CommonModule } from '@angular/common';\n"
    st += "import { MaterialModule" + sx1 +" } from 'charms-lib';\n"
    st += "import { Routes, RouterModule } from '@angular/router';\n"
    st += "import { FormsModule, ReactiveFormsModule } from '@angular/forms';\n"
    st += "import { "+ model +"FormComponent } from './"+ slug +".component';\n"
    st += "\n"
    st += "const routes: Routes = [\n"
    st += "    { path: '', component: "+ model +"FormComponent},\n"
    st += "];\n"
    st += "\n"
    st += "@NgModule({\n"
    st += "    imports: [\n"
    st += "        // Angular Core Modules\n"
    st += "        CommonModule,\n"
    st += "        FormsModule,\n"
    st += "        ReactiveFormsModule,\n"
    st += "\n"
    st += "        RouterModule.forChild(routes),\n"
    st += "\n"
    st += "        // Library;\n"
    st += "        MaterialModule,\n"
    st += sx2
    st += "    ],\n"
    st += "    declarations: [\n"
    st += "        "+ model +"FormComponent\n"
    st += "    ],\n"
    st += "    exports: [\n"
    st += "        "+ model +"FormComponent\n"
    st += "    ],\n"
    st += "    providers: [],\n"
    st += "})\n"
    st += "\n"
    st += "export class "+ model +"FormModule { }\n"
    st += "\n"

    f = open(codePath + 'module.ts', 'w')
    print(st, file=f)

def writeCustomForm(codePath, configTables, tableDict):
    print("\n=========================================================")
    print("========= Generating Custom Form Angular =================")
    print("=========================================================")
    
    path = codePath
    mkdir(path)

    for table in configTables:
        if "customForm" in table:
            path = codePath + table["slug"] + "/"
            mkdir(path)

            print("Writing CustomForm for = ", table["modelName"], "(" + str(len(table["columns"])) + ")")
            
            #exit()
            writeCustomFormHTML(table, path)
            writeCustomFormTS(table, path, configTables, tableDict)
            # writeCustomFormSCSS(table, path)
            # writeCustomFormModule(table, path)

        # if "itemForm" in table:

