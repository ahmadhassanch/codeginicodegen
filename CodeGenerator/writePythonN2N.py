from .utils import findTableFromTableName, findPrimaryColumn2
def getMYID(currentTable, table):
    for column in table["columns"]:
        if ('invisible' in column["props"]):
            continue

        if column["ctype"] == "ForeignKey":
            if column["addition_params"]["ForeignTable_s"] == currentTable:
                return column['name']


def writeCreateN2N():
    st = ""
    st += "\n"
    st += "    # Override create function\n"
    st += "    async def Create(self):\n"
    st += "        id = await super().Create()\n"
    st += "        self.add_many2many_details(id['data'])\n"
    return st    

def writeUpdateN2N(tableData, configTables):
    st = ""
    st += "    \n"
    st += "    async def Update(self):\n"
    st += "        id = await super().Update()\n"
    st += "        id = id['data']\n"
    # print("================================================================\n");
    for item in tableData["customForm"]:
        if item["type"] == "many2many" or item["type"] == "itemForm":
            title_s = item['n2nTable'];
            table = findTableFromTableName(title_s, configTables)
            modelName = table["modelName"] 
            name = getMYID(tableData["title_s"], table)
            st += "        # Delete old " +table["title_p"]+ "\n"
            st += "        self.gdb.query("+modelName+").filter("+modelName+"."+name+" == id).delete()\n"
    st += "        # Update leaf tables\n"
    st += "        self.add_many2many_details(id)\n"
    return st

def writeN2Ndetails(tableData, configTables):
    st = ""
    st += "\n    def add_many2many_details(self, id):\n"
    for item in tableData["customForm"]:
        if item["type"] == "many2many"  or item["type"] == "itemForm":
            title_s = item['n2nTable'];
            table = findTableFromTableName(title_s, configTables)
            modelName = table["modelName"] 
            name = getMYID(tableData["title_s"], table)

            # mid = getMYID(item["otherTable"], table)
            mid = item["n2nColumn"]
            st += "\n        # fetch "+modelName+" list from post data\n"
            #st += "        list = self.Data['"+table["tableName_c"]+"']\n"
            st += "        list = self.Data['items']\n"
            st += "        for item in list:\n"
            st += "            form = "+modelName+"()\n"
            # st += "            form."+mid+" = item['"+mid+"']\n"  
            st += "            form."+ name +" = id\n"

            
            for column in table["columns"]:
                if("invisible" in column["props"]): continue
                if(column["name"] == name): continue
                # mid
                st += "            form."+column["name"] + " = item['"+column["name"]+"']\n"

            st += "            form.created_by_id = self.Session.employee_id\n"
            st += "            form.last_edited_by_id = self.Session.employee_id\n"
            st += "            self.gdb.add(form)\n"
    return st    


def writeSingleN2N(tableData, configTables):
    st = ""
    pKey = findPrimaryColumn2(tableData["columns"])[1];
    print(pKey)
    st += "\n"
    st += "    async def Single(self):\n"
    st += "        data = await super().Single()\n"
    st += "        data = data['data']\n"

    for item in tableData["customForm"]:
        st += "\n"
        if item["type"] == "many2many"  or item["type"] == "itemForm":
            title_s = item['n2nTable'];
            table = findTableFromTableName(title_s, configTables)
            modelName = table["modelName"] 
            name = getMYID(tableData["title_s"], table)

            st += "        if '"+ item['otherTable'] +"' in self.Data:\n"
            st += "            "+item['otherTable']+" = []\n"
            st += "            # TODO: make this query async\n"
            st += "            for item in self.gdb.query("+ modelName+").filter(\n"
            st += "                    "+ modelName+"."+pKey+" == self.Data['oid']).all():\n"
            st += "                if len(self.Data['"+item['otherTable']+"']) == 0:\n"
            st += "                    "+item['otherTable']+".append({\n"
            title_s = item['n2nTable'];
            table = findTableFromTableName(title_s, configTables)
            for column in table["columns"]:
                if("invisible" in column["props"]): continue
                if(column["name"] == name): continue
                # mid
                st += "                        '"+column["name"] + "' : item."+column["name"]+",\n"

            st += "                    })\n"
            st += "                else:\n"
            st += "                    try:\n"
            st += "                        item_to_append = {}\n"
            st += "                        for key in self.Data['"+item['otherTable']+"']:\n"
            st += "                            item_to_append[key] = getattr(item, key)\n"
            st += "                        "+item['otherTable']+".append(item_to_append)\n"
            st += "                    except AttributeError as error:\n"
            st += "                        raise ExceptionWithMessage(error, 1205)\n"
            st += "            if len("+item['otherTable']+") > 0:\n"
            st += "                data['"+item['otherTable']+"'] = "+item['otherTable']+"\n"
    st += "        return {'data': data}"
    return st





def pythonMany2Many(tableData, configTables):
    st = writeCreateN2N()
    st += writeUpdateN2N(tableData, configTables)
    st += writeN2Ndetails(tableData, configTables)
    st += writeSingleN2N(tableData, configTables)
    return st



    # async def Single(self):
    #     is_formulas = False
    #     if 'formulas' in self.Data['columns']:
    #         self.Data['columns'].remove('formulas')
    #         is_formulas = True

    #     is_labels = False
    #     if 'labels' in self.Data['columns']:
    #         self.Data['columns'].remove('labels')
    #         is_labels = True

    #     data = await super().Single()
    #     data = data['data']

    #     if is_formulas:
    #         formulas = []
    #         for formula in self.gdb.query(InventoryFormula).filter(InventoryFormula.inventory_id == self.Data['oid']).all():
    #             formulas.append({'formula_name': formula.formula.formula_name, 'formula_id': formula.formula.formula_id})

    #         if len(formulas) > 0:
    #             data['formulas'] = formulas

    #     if is_labels:
    #         labels = []
    #         for label in self.gdb.query(InventoryLabel).filter(InventoryLabel.inventory_id == self.Data['oid']).all():
    #             labels.append({'label_name': label.label.label_name, 'label_id': label.label.label_id})

    #         if len(labels) > 0:
    #             data['labels'] = labels

    #     return {'data': data}



    # def writeSingleN2N1(tableData, configTables):
    # st = "\n"
    # st += "    async def Single(self):\n"

    # for item in tableData["customForm"]:
    #         if item["type"] == "many2many":

    #             title_s = item['n2nTable'];
    #             otherTable = item['otherTable'];
    #             st += "\n        is_" + otherTable + " = False\n"
    #             st += "        if '" + otherTable + "' in self.Data['columns']:\n"
    #             st += "            self.Data['columns'].remove('"+otherTable+"')\n"
    #             st += "            is_"+otherTable+" = True\n"
    
    # st += "\n        data = await super().Single()\n"
    # st += "        data = data['data']\n"

    # for item in tableData["customForm"]:
    #         if item["type"] == "many2many":

    #             title_s = item['n2nTable'];
    #             otherTable = item['otherTable'];
    #             table = findTableFromTableName(title_s, configTables)
    #             modelName = table["modelName"] 
    #             name = getMYID(tableData["title_s"], table)
    #             st += "\n        if is_" + otherTable + ":\n"
    #             st += "            " + otherTable + " = []\n"
    #             relationName = item["n2nColumn"][:-3]
    #             nm = item["otherTableChipColumns"][0]
    #             nid = item["n2nColumn"]
    #             st += "            for item in self.gdb.query("+modelName+").filter("+modelName+"."+name+" == self.Data['oid']).all():\n"
    #             st += "                "+otherTable+".append({'"+nm+"': item."+relationName+"."+nm+", '"+nid+"': item."+relationName+"."+nid+"})    \n\n"
    #             st += "            if len("+otherTable+") > 0:\n"
    #             st += "                data['"+otherTable+"'] = "+otherTable+"\n"
    #             # st += "        if '" + otherTable + "' in self.Data['columns']:\n"
    #             # st += "            self.Data['columns'].remove('"+otherTable+"')\n"
    #             # st += "            is_"+otherTable+" = True\n"
    # st += "\n        return {'data': data}\n"
    # return st    
