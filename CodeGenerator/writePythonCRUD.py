from .utils import findPrimaryColumn2, fileExists, findTableFromTableName
from .utils import mkdir, updateCodeBeforeTag, getStringParams, getScalarParams
from .writePythonN2N import pythonMany2Many

def imports():
    st = ""
    st += "from sqlalchemy import BigInteger, Boolean, ForeignKey, Integer, String, Float, Text"
    st += "\nfrom sqlalchemy.orm import relationship, column_property"
    st += "\nfrom sqlalchemy.sql import func"
    st += "\nfrom chi.data.base_models import ChiBase, DEFAULT_TABLE_ARGS, CGColumn, Serializer, Json"
    st += "\nfrom chi.data.system_models import Employee\n"
    return st

def writeCommonInfo():
    # return "\n"
    st = "\n"
    st += "    is_deleted = CGColumn(Boolean, nullable=False, default=False)\n"
    st += "    delete_comments = CGColumn(String(512), nullable=True)\n\n"
    st += "    created_by_id = CGColumn(ForeignKey(Employee.employee_id, onupdate='RESTRICT', ondelete='RESTRICT'),nullable=False)\n"
    st += "    created_by = relationship(Employee, remote_side=Employee.employee_id, foreign_keys=created_by_id, lazy='select')\n\n"
    st += "    last_edited_by_id = CGColumn(ForeignKey(Employee.employee_id, onupdate='RESTRICT', ondelete='RESTRICT'),nullable=False)\n\n"
    st += "    date_added = CGColumn(BigInteger, nullable=False, default=func.unix_timestamp())\n"
    st += "    date_updated = CGColumn(BigInteger, nullable=False, default=func.unix_timestamp(),onupdate=func.unix_timestamp())\n "
    return st

def writeGeneralInfo(column):
    st = ""
    if "primary_key" in column["props"]:
        st += ", primary_key = True"
    if "autoincrement" in column["props"]:
        st += ", autoincrement = True"
    if "required" in column["props"]:
        st += ", nullable = False"
    if "unique" in column["props"]:
        st += ", unique = True"
    return st

def writeIntegerColumn(column):
    st = ""
    st += "    " + column["name"] + " = CGColumn(" + column["ctype"]
    st += writeGeneralInfo(column)
    st += ")\n"
    return st

def writeFloatColumn(column):
    st = ""
    st += "    " + column["name"] + " = CGColumn(" + column["ctype"]
    st += writeGeneralInfo(column)

    if "default" in column["addition_params"]:
        st += ", default = " + str(column["addition_params"]["default"])

    st += ")\n"
    return st

def writeBoolColumn(column):
    st = ""
    st += "    " + column["name"] + " = CGColumn(" + column["ctype"]
    st += writeGeneralInfo(column)

    if "default" in column["addition_params"]:
        st += ", default = " + str(column["addition_params"]["default"])

    st += ")\n"
    return st

def writeStringColumn(column):
    st = ""
    st += "    " + column["name"] + " = CGColumn(" + column["ctype"]
    st += "(" + str(column["addition_params"]["length"]) + ")"
    st += writeGeneralInfo(column)
    st += ")\n"
    return st

def writeTextColumn(column):
    st = ""
    st += "    " + column["name"] + " = CGColumn(" + column["ctype"]
    st += writeGeneralInfo(column)
    st += ")\n"
    return st

#"    date_updated = CGColumn(BigInteger, nullable=False, default=func.unix_timestamp(),onupdate=func.unix_timestamp())\n "
def writeDateColumn(column):
    st = ""
    st += "    " + column["name"] + " = CGColumn(BigInteger" 
    #st += writeGeneralInfo(column)
    st += ")\n"
    return st

def writeForeignKeyColumn(column, tableConfig):
    modelName = column["addition_params"]["externalModel"]
    externalColumn = column["addition_params"]["externalColumn"]
    columnName = column["name"]
    st = ""
    st += "    " + columnName + " = CGColumn(" + column[
        "ctype"]

    st += "(" + modelName + "."
    st += externalColumn
    if "onupdate" in column["addition_params"]:
        st += ", onupdate = " + "'" + column["addition_params"]["onupdate"] + "'"

    if "ondelete" in column["addition_params"]:
        st += ", ondelete = " + "'" + column["addition_params"]["ondelete"] + "'"

    st += ")"

    st += writeGeneralInfo(column)
    st += ")\n"

    relationName = columnName[:-3]
    st += "    " + relationName + " = relationship("
    st += modelName + ","
    st += "remote_side = " + modelName + "."  ######
    st += externalColumn + ", "
    # + modelName + "."
    st += "foreign_keys = " + columnName + ", "
    st += "lazy='select'"

    st += ")\n"
    return st

def findModelName(configTables, tableName):
    for table in configTables:
        return table["modelName"]
    return False, 0

def writePythonModel(fileName, tableData, configTables, moduleName):

    st = "class "
    st += tableData["modelName"] + "(ChiBase, Serializer):\n"
    st += "    __tablename__ = " + '"' + tableData["tableName"] + '"' + "\n"

    includesDict = {};

    flag, pKey = findPrimaryColumn2(tableData["columns"])
    if flag:
        st += "    __primary_key__ = " + '"' + pKey + '"' + "\n"
    st += "    __table_args__ = DEFAULT_TABLE_ARGS\n\n"

    for column in tableData["columns"]:
        if column["ctype"] == "Integer":
            st += writeIntegerColumn(column)

        if column["ctype"] == "String":
            st += writeStringColumn(column)

        if column["ctype"] == "ForeignKey":
            st += writeForeignKeyColumn(column, configTables)
            includesDict[column["addition_params"]["externalTable_c"]] = column["addition_params"]["externalModel"]

        if column["ctype"] == "Float":
            st += writeFloatColumn(column)

        if column["ctype"] == "Boolean":
            st += writeBoolColumn(column)

        if column["ctype"] == "Text":
            st += writeTextColumn(column)

        if column["ctype"] == "Date":
            st += writeDateColumn(column)

    st += writeCommonInfo()

    st1 = imports()
    
    for key in includesDict:
        st1 += "from chi.data." + moduleName + "." + key
        st1 += " import " + includesDict[key] + "\n"

    f = open(fileName, 'w')
    print(st1 + "\n\n" + st, file=f)




def writePythonModule(fileName, tableData, configTables, tableDict):

    modelName = tableData["modelName"]
    tableName = tableData["tableName_c"]
    slug = tableData["slug"]
    st = "from chi.common import CGBaseController\n"
    st += "from chi.data import " + modelName
    if 'customForm' in tableData:

            
        for item in tableData["customForm"]:

            if item["type"] == 'many2many' or item["type"] == 'itemForm' :
                table = findTableFromTableName(item["n2nTable"], configTables)
                st += ", "+table["modelName"]
            # elif item["type"] == "itemForm":
            #     table = findTableFromTableName(item["itemTable"], configTables)
            #     st += ", "+ table["modelName"]
            else:
                print("unknown form type");
                exit();
        st += '\nfrom chi.common.exceptions import ExceptionWithMessage, QuantityExceedsLimit'

    st += "\n\n\n"

    st +=  "class " + modelName + "Controller(CGBaseController):\n"

    st += "    def prepare(self):\n"
    st += "        self.Model = "+modelName+"\n"
    st += "        self.Slug = '"+tableName+"'\n"
    st += "    \n"
    st += "        super().prepare()\n"

    # mst = "\n\n ======  \n"
    if 'customForm' in tableData:
        # for item in tableData["customForm"]:
        if item["type"] == 'many2many'  or item["type"] == 'itemForm' :
            st += pythonMany2Many(tableData, configTables)
        else:
            print("unknown form type");
            exit();

    f = open(fileName, 'w')
    print(st, file=f)


def createDataInitFile(filePath):
    if not (fileExists(filePath)):
        st = "from .base_models import CurrentSession\n\n"
        st += "##RTAG <<Add model imports, do not remove>>"
        f = open(filePath, 'w')
        print(st, file = f)

def createModulesInitFile(filePath):
    if not (fileExists(filePath)):
        st = "##RTAG <<Add controller imports, do not remove>>\n"
        st += "\n"
        st += "def get_registry():\n"
        st += "    reg = {\n"
        st += "##RTAG <<Register Controllers, do not remove>>\n"
        st += "    }\n"
        st += "    return reg\n"

        f = open(filePath, 'w')
        print(st, file = f)


def writePythonCRUD(codePath, module, tableDict):
    configTables = module["tables"];
    moduleName = module["name"];
    print("\n=========================================================")
    print("=========== Generating Backend APIs =====================")
    print("=========================================================")
    
    dataPath = codePath + "chi/data/"
    mkdir(dataPath)
    modulePath = codePath + "chi/modules/"
    mkdir(modulePath)
    
    dataInitFilePath = dataPath  + '__init__.py'
    createDataInitFile(dataInitFilePath)

    moduleInitFilePath = modulePath  + '__init__.py'
    createModulesInitFile(moduleInitFilePath)

    count = 0
    for table in configTables:
        tableName = table['tableName'][4:];
        count += 1
        print(count, " - Writing Backend for: ", tableName +".py")

        modelName = table["modelName"]
        line = "from ."+moduleName+"."+ tableName +" import " + modelName + ""      #TODO: if different model
        #updateCodeBeforeTag(initFilePath, "## <<Add model imports, do not remove>>", ", "+ modelName, False)
        updateCodeBeforeTag(dataInitFilePath, "##RTAG <<Add model imports, do not remove>>", line, True)
        
        line = "from ." +moduleName+"."+ table["tableName_c"] + " import " + modelName + "Controller"
        updateCodeBeforeTag(moduleInitFilePath, "##RTAG <<Add controller imports, do not remove>>", line, True)

        line = "        '" +  table["tableName_c"] + "' : " + modelName + "Controller,"
        updateCodeBeforeTag(moduleInitFilePath, "##RTAG <<Register Controllers, do not remove>>", line, True)

        filePath = dataPath + moduleName + "/" 
        mkdir(filePath)
        fileName = filePath +tableName + '.py'

        initFile = filePath + "__init__.py"
        if not fileExists(initFile):
            f = open(initFile, 'w')
            print("", file = f)
        writePythonModel(fileName, table, module, moduleName)
        
        filePath = modulePath + moduleName + "/" 
        mkdir(filePath)
        fileName = filePath + tableName + '.py'
        writePythonModule(fileName, table, configTables, tableDict)

