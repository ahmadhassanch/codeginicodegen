from pathlib import Path
import os.path
from os import path

def fileExists(filePath):
    if path.exists(filePath):
        return True
    else:
        return False
        
def mkdir(path):
    Path(path).mkdir(parents=True, exist_ok=True)

def findPrimaryColumn2(columns):
    # print("============ start ++++++++++++")
    for column in columns:
        # print(column["props"])
        if "primary_key" in column['props']:
            # print("==========yes", column["name"])
            return True, column["name"]
    return False, 0

# This function will insert the code immediately before the given TAG in the file (filePath)
# TODO: Check if the file exists, if it does not exist, error and exit/assert
#
# Normally we will append in new line (by default), 
# if we want to append at the end of line, set newLine = False
def updateCodeBeforeTag(filePath, tag, insertedCode, newLine = True):
    if(newLine == True):
        if insertedCode[-1]!= "\n":
            insertedCode += "\n"
    # print(filePath)
    file = open(filePath, "r")
    lineData = file.readlines(); 
    st = "";
    for line in lineData:
        if line == insertedCode: return    #dont repeat the code if it already exists
        #print("--", line)
        if(line[:-1] == tag):
            if newLine == True:
                st += insertedCode;
            else:
                stNew = st[:-1]
                stNew += insertedCode + "\n"
                st = stNew
        st += line;
    f = open(filePath , 'w')
    print(st[:-1], file=f)

def getStringParams(st):

    sts = st.split("=")
    arr = [];
    for v in sts:
        v1 = v.strip()
        arr.append(v1)

    stringLength = arr[0][7:-1]
    if len(arr) > 2:
        print("Error in String Column initialization: ", st)
        exit()
    
    defaultValue = None
    if (len(arr)>1):
        defaultValue = arr[1];
    # print(stringLength, defaultValue)
    return stringLength, defaultValue


def getScalarParams(st):

    sts = st.split("=")
    arr = [];
    for v in sts:
        v1 = v.strip()
        arr.append(v1)

    if len(arr) > 2:
        print("Error in Float Column initialization: ", st)
        exit()
    
    defaultValue = None
    if (len(arr)>1):
        defaultValue = arr[1];
    #print("default value  = ", defaultValue, arr)
    return defaultValue


def findTableFromTableName(tableName, configTables):
    for table in configTables:
        if(table['title_s'] == tableName):
            return table
