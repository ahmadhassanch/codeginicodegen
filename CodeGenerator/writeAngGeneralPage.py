from .utils import findPrimaryColumn2, mkdir, fileExists
from .utils import updateCodeBeforeTag, getStringParams, getScalarParams
import time

def writeActions(tableData):
    st = ""
    for actions in tableData["rowActions"]:
        start = 0;
        st += "            {"
        for action in actions:
            if(start != 0): st+=", "
            st += action + " : " + "'" + actions[action] + "'"
            start += 1
        st += "},"
        st += "  \n"

    st += "        ],\n\n"
    return st

def writeTable(tableData):
    st = "\n"
    st += "        table : {\n"
    st += "            columns : [\n"
    for column in tableData["columns"]:

        if column["ctype"] == "ForeignKey":
            
            params = column["addition_params"]
            displayColumns = params["externalColumnDisplay"]
            for key in displayColumns:
                st += "                { name : "
                columnName = column["name"]
                relationName = columnName[:-3]
                st += "'" + relationName + "." + displayColumns[key] + "'"
                st += ", title : " + "'" + key + "'},\n"

        else:
            st += "                { name : "
            st += "'" + column["name"] + "'"

            if "invisible" in column["props"]:
                st += ", visible: false"
            else:
                st += ", title : " + "'" + column["title"] + "'"

            if "showInSearch" in column["props"]:
                st += ", showInSearch : true"

            if "format" in column:
                st += ", format : " + "'" + column["format"] + "'"

            if "sortable" in column["props"]:
                st += ", sortable : true"

            if "sticky" in column["props"]:
                st += ", sticky : true"

            if "showFilter" in column["props"]:
                st += ", showFilter : true"
            st += "},\n"

    st += "            ],\n"
    st += "\n            order: {column: 'date_added', dir: 'desc'},\n"
    st += "        },"
    return st

def writeForm(tableData):
    st = "\n\n"
    st += "        form : {\n"
    st += "            columns : [\n"
    for column in tableData["columns"]:

        st += "                { name : " + "'" + column["name"] + "'"
        # print(f"++++++++++++++++{st}++++++++++++++++")
        if "invisible" in column['props']:
            st += ", type : 'hidden'"
        else:
            st += ", title : " + "'" + column["title"] + "'"

        if "required" in column["props"]:
            st += ", required : true"

        if column["ctype"] == "ForeignKey":
            st += ", type: 'foreign'"
            params = column["addition_params"]
            st += ",\n                    foreign: {\n"
            st += "                           foreign_table : " + "'" + params["externalTable_c"] + "'"
            st += ", foreign_column : " + "'" + params["externalColumn"] + "'"
            if 'mode' in params:
                st += ", mode : " + "'" + params["mode"] + "'"

            st += ",\n                           columns : ["
            for cols in params["dropdownColumns"]:
                st += "'" + cols + "'" + ", "
            st += "] \n                  }"
            st += "\n                "
        if column["ctype"] == "String":
            st += ", type : 'text'"

        if column["ctype"] == "Boolean":
            st += ", type : 'switch'"
        st += "},\n"

    st += "            ],\n"
    st += "        },"
    return st

def writeAngularModel(tableData, moduleName):
    st = "export const " + tableData["modelName"] + "ControllerConfig = {\n"

    st += "        slug : '" + tableData["tableName_c"] + "',\n"
    flag, pKey = findPrimaryColumn2(tableData["columns"])

    st += "        key : '" + pKey + "',\n"
    st += "        title_s : '" + tableData["title_s"] + "',\n"
    st += "        title_p : '" + tableData["title_p"] + "',\n\n"

    st += "        showHeader: true,\n"
    st += "        showSearch: true,\n"
    st += "        showAdd: true,\n"
    st += "        showImport: false,\n"
    st += "        showRowActions: true,\n\n"
    st += "        inRoute: true,\n\n"
    st += "        rowActions: [\n"
    st += writeActions(tableData)
    st += writeTable(tableData)
    st += writeForm(tableData)

    st += "\n    \n};"
    return st

def createIndexFile(filePath):
    if not (fileExists(filePath)):
        st = "//RTAG <<Add ControllerConfig1, do not remove>>\n"
        st += "\n"
        st += "export const ControllerConfigs = [\n"
        st += "  \n"
        st += "//RTAG <<Add ControllerConfig2, do not remove>>\n"
        st += "];\n"

        f = open(filePath, 'w')
        print(st, file = f)

def createModuleFile(filePath):
    if not (fileExists(filePath)):
        st = ""
        st += "import { NgModule } from '@angular/core';\n"
        st += "import { CommonModule } from '@angular/common';\n"
        st += "import { MaterialModule, GeneralModule, GeneralPageComponent, GeneralFormComponent } from 'charms-lib';\n"
        st += "import { Routes, RouterModule } from '@angular/router';\n"
        st += "\n"
        st += "const routes: Routes = [\n"
        st += "//RTAG <<Add to Routes, do not remove>>\n"
        st += "];\n"
        st += "\n"
        st += "@NgModule({\n"
        st += "    imports: [\n"
        st += "        CommonModule,\n"
        st += "        MaterialModule,\n"
        st += "        GeneralModule,\n"
        st += "\n"
        st += "        RouterModule.forChild(routes)\n"
        st += "    ],\n"
        st += "    declarations: [\n"
        st += "    ],\n"
        st += "    exports: [\n"
        st += "    ],\n"
        st += "    providers: [],\n"
        st += "})\n"
        st += "export class AccountsModule { }\n"

        f = open(filePath, 'w')
        print(st, file = f)

def createNavIndexFile(filePath):
    if not (fileExists(filePath)):
        st = ""

        st += "import { FuseNavigation } from 'charms-lib';\n"
        st += "\n"
        st += "\n"
        st += "export const navigation: FuseNavigation[] = [\n"
        st += "    {\n"
        st += "        id       : 'dashboard',\n"
        st += "        title    : 'Dashboard',\n"
        st += "        type     : 'item',\n"
        st += "        icon     : 'dashboard',\n"
        st += "        url      : '/main-page',\n"
        st += "        children : []\n"
        st += "    },\n"
        st += "    {\n"
        st += "        id       : 'accounts',\n"
        st += "        title    : 'Accounts',\n"
        st += "        type     : 'collapsable',\n"
        st += "        icon     : 'event_note',\n"
        st += "        badge    : {\n"
        st += "            title: '20',\n"
        st += "            bg: 'red',\n"
        st += "            fg: 'white'\n"
        st += "        },\n"
        st += "        children : [\n"
        st += "//RTAG <<Add to Routes, do not remove>>\n"
        st += "        ]\n"
        st += "    }\n"
        st += "];\n"

        f = open(filePath, 'w')
        print(st, file = f)

def writeAngGeneralPage(codePath, module):
    configTables = module["tables"];
    print("\n=========================================================")
    print("========= Generating Frontend Angular ===================")
    print("=========================================================")
    #time.sleep( .25 )
    
    path = codePath + "configs/"
    mkdir(path)

    indexFilePath = path + "index.ts"
    createIndexFile(indexFilePath)

    moduleFilePath = codePath + "pages/accounts/"
    mkdir(moduleFilePath)

    moduleFileName = moduleFilePath + "module.ts"
    createModuleFile(moduleFileName)
    
    navPath = codePath + "core/navigation/"
    mkdir(navPath)
    navFilePath = navPath + "index.ts" 
    createNavIndexFile(navFilePath)
    moduleName = module["name"];

    count = 0;
    for table in configTables:
        #time.sleep( .05 )
        count += 1
        print(count, " - Writing Angular for = ", table["modelName"], "(" + str(len(table["columns"])) + ")")
        v = table['tableName_c'];
        fPath = path + moduleName+"/";
        mkdir(fPath)
        f = open(fPath + v + '.ts', 'w')
        modelName = table["modelName"]
        tableName = table["tableName_c"]
        slug = table["slug"]
        s = writeAngularModel(table, moduleName)
        print(s, file=f)

        line = "import { " + modelName + "ControllerConfig } from './" + moduleName+"/"+ tableName +  "'"
        updateCodeBeforeTag(indexFilePath, "//RTAG <<Add ControllerConfig1, do not remove>>", line, True)

        line = "    {name :'" + slug + "', ctrl : " + modelName + "ControllerConfig},"
        updateCodeBeforeTag(indexFilePath, "//RTAG <<Add ControllerConfig2, do not remove>>", line, True)

        line = "    { path :'" + slug + "', component: GeneralPageComponent, data: {name: '" + slug + "'} },"
        updateCodeBeforeTag(moduleFileName, "//RTAG <<Add to Routes, do not remove>>", line, True)

        text =  "          {"
        text += "id: '"+ slug +"', "
        text += "title: '"+ table["title_p"] +"', "
        text += "type: 'item', "
        text += "url: '/accounts/"+slug+"'"
        text += "},\n"

        line = text
        updateCodeBeforeTag(navFilePath, "//RTAG <<Add to Routes, do not remove>>", line, True)
