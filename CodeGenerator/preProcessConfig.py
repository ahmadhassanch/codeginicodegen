from CodeGenerator.utils import mkdir, updateCodeBeforeTag, getStringParams, getScalarParams

def preProcessConfig(module):
    modulePrefix = module["prefix"]
    configTablesX = module["tables"];
    tableDict = {};
    for table in configTablesX:
        # tableName = table["name"]
        title_s = table["title_s"]
        title_p = table["title_p"]
        table["tableName"] = modulePrefix + title_p.replace(" ", "_").lower()
        table["tableName_c"] = title_p.replace(" ", "_").lower()  #without prefix
        table["modelName"] = title_s.replace(" ", "")
        #print(">>>>"+ table["modelName"]+"--")
        table["slug"] = title_p.replace(" ", "-").lower()
        tableDict[table["title_s"]] = {
            "tableName" :  table["tableName"],
            "tableName_c" :  table["tableName_c"],
            "title_s"   :  table["title_s"],
            "title_p"   :  table["title_p"],
            "slug"      :  table["slug"],
            "modelName" :  table["modelName"],
        }
    # exit()

                    # "ForeignTable_s": "Account Type",
                    # "ForeignColumn": "account_type_id",
                    # "externalModel": "AccountType",
                    # "externalModelSlug": "account_types",
                    # "externalTable": "account_type",
                    # "externalColumn": "account_type_id",

    for table in configTablesX:
        for column in table["columns"]:
            if(column["ctype"] == "ForeignKey"):

                ftableName = column["addition_params"]["ForeignTable_s"];
                ftable = tableDict[ftableName]
                column["addition_params"]["externalModel"] =  ftable["modelName"]

                mName = ftable["modelName"];
                mName = mName[0].lower() + mName[1:]

                column["addition_params"]["externalModelCamel"] =  mName
                column["addition_params"]["externalTable_c"] =  ftable["tableName_c"]
                column["addition_params"]["externalModelSlug"] = ftable["slug"]
                column["addition_params"]["externalTable"] = ftableName
                column["addition_params"]["externalColumn"] = column["addition_params"]["ForeignColumn"]
                column["addition_params"]["onupdate"] =  "RESTRICT"
                column["addition_params"]["ondelete"] =  "RESTRICT"

            if not ("props" in column):
                column["props"] = []

            if(column["ctype"][:6] == 'String'):
                stringLength, defaultValue = getStringParams(column["ctype"])

                column["ctype"] = 'String'
                column["addition_params"] = {"length": stringLength}

            if(column["ctype"][:5] == 'Float'):
                defaultValue = getScalarParams(column["ctype"])

                column["ctype"] = 'Float'
                column["addition_params"] = {"default": defaultValue}

            if(column["ctype"][:7] == 'Boolean'):
                defaultValue = getScalarParams(column["ctype"])

                column["ctype"] = 'Boolean'
                column["addition_params"] = {"default": defaultValue}
    
    return tableDict;

def outputCompactConfig(module):
    modulePrefix = module["prefix"]
    configTablesX = module["tables"];
    moduleName = module["name"]

    st = ""
    st += "Tables = {}\n"
    st += "\n"
 
    spi = "            ";
    for table in configTablesX:
        st += '\nTables["' +table["title_s"]+'"] = {\n'
        st += '    "title_p" : "'+table["title_p"]+'",\n'
        st += '    "columns" : {\n'
        for column in table["columns"]:
            st += spi
            #if(column["ctype"] == "ForeignKey"):
            st += '"'+column["name"] +'"'+ ' : ['
            # st += '"'+column["ctype"]+'"'

            if column["ctype"] == "ForeignKey":
                st += '"FK"'
                st += ', "'+column["addition_params"]["ForeignTable_s"] + '"'
                st += ', "'+column["addition_params"]["ForeignColumn"] + '"'
                

            if column["ctype"] == "Integer":
                st += '"' + column["ctype"] + '"'

            if column["ctype"] == "Boolean":
                st += '"' + column["ctype"] + '"'

            if column["ctype"] == "Float":
                st += '"' + column["ctype"] + '"'

            if column["ctype"] == "Text":
                st += '"' + column["ctype"] + '"'

            if column["ctype"] == "String":
                st += '"' + column["ctype"] + '('+column["addition_params"]["length"]+ ')"'

            for value in column["props"]:
                if (value == "primary_key"):
                    st += ', "PK"'
                elif (value == "autoincrement"):
                    st += ', "AI"'
                elif (value == "required"):
                    st += ', "REQUIRED"'
                elif (value == "invisible"):
                    st += ', "HIDDEN"'
                elif (value == "showInSearch"):
                    st += ', "showInSearch"'
                                

                #column["ctype"] == "ForeignKey":
                #    st += "[PK]"
            st += '],\n'

        
        st += "     }\n"
        
    st += "}\n"
    return st
    #print(st)
