from CodeGenerator.utils import mkdir, updateCodeBeforeTag, getStringParams

title_s = "Account Type"
title_p = "Account Types"
tableName = title_s.replace(" ", "_").lower()
modelName = title_s.replace(" ", "")
slug = title_s.replace(" ", "-").lower()

print("title  = ", title_s)
print("tableName  = ", tableName)
print("modelName  = ", modelName)
print("slug  = ", slug)


st = "String(124)    ";
getStringParams(st)


