import { FuseNavigation } from 'charms-lib';


export const navigation: FuseNavigation[] = [
    {
        id       : 'dashboard',
        title    : 'Dashboard',
        type     : 'item',
        icon     : 'dashboard',
        url      : '/main-page',
        children : []
    },
    {
        id       : 'accounts',
        title    : 'Accounts',
        type     : 'collapsable',
        icon     : 'event_note',
        badge    : {
            title: '20',
            bg: 'red',
            fg: 'white'
        },
        children : [
//RTAG <<Add to Routes, do not remove>>
        ]
    }
];
