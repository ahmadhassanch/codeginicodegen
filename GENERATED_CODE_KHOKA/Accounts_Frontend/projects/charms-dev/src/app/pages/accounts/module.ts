import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule, GeneralModule, GeneralPageComponent, GeneralFormComponent } from 'charms-lib';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
//RTAG <<Add to Routes, do not remove>>
];

@NgModule({
    imports: [
        CommonModule,
        MaterialModule,
        GeneralModule,

        RouterModule.forChild(routes)
    ],
    declarations: [
    ],
    exports: [
    ],
    providers: [],
})
export class AccountsModule { }
