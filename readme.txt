
//text

  <mat-form-field fxFlex="30" appearance="outline">
    <mat-label>Description</mat-label>
    <input matInput type="text" formControlName="description" />
  </mat-form-field>
  
//number

  <mat-form-field fxFlex="30" appearance="outline">
    <mat-label>Storage to Selling</mat-label>
    <input matInput type="number" formControlName="storage_to_selling" />
  </mat-form-field>


//Boolean DropDown

  <mat-form-field fxFlex="30" appearance="outline">
    <mat-label>Expiry Date Enabled</mat-label>
    <mat-select formControlName="expiry_date_enabled">
      <mat-option [value]="true"> Yes </mat-option>
      <mat-option [value]="false"> No </mat-option>
    </mat-select>
  </mat-form-field>
  
//foreign

  <mat-form-field fxFlex="30" appearance="outline">
    <mat-label>Manufacturer</mat-label>
    <chi-select [field]="manufacturerId"></chi-select>
  </mat-form-field>
  
  
 //n2n select
 
   <chi-n2n-select
    fxFlex="30"
    [showAll]="true"
    [field]="formulas"
  ></chi-n2n-select>
  
  
 //Date Picker
 
   <mat-form-field fxFlex="30" appearance="outline">
    <mat-label>Date</mat-label>
    <chi-date-picker [field]="date"></chi-date-picker>
  </mat-form-field>
  
 //Checkbox
 
   <div fxFlex="30" fxLayoutAlign="start center">
    <mat-checkbox  formControlName="narcotics"
      >Narcotics</mat-checkbox
    >
  </div>
- The controllers of each module have their corresponding module prefix.

- Plan: 
> Create the files if they do not exist (in which we enter specific lines)

Ali / Sarah
- Handle other types of fields (Float, Text, Date, ....)
  > HTML and TS

- fix the date stuff in .ts

- Selector should be unique, right now we are writing custom form.

- do not repeat MULTI-LINE code if it already exists in the file. (e.g. for menu iterms)
  > already handled single line



INVENTORY SYSTEM IMPLEMENTATION:
================================

INVENTORY CUSTOMIZATION LIST OF THINGS TO CUSTOMIZE
---------------------------------------------------
- List prepared of what customization need to be done

CodeGenerator:
--------------
- Ahmad (with help of Ali, Sara)

Inventory Tables:
- Sarfraz

Angular/Backend Training:
-------------------------
- Using General Table and General Form in a Custom Page
- Using an off-the shelf component in a Custom Page



=====================================
The 10 rules of Programming
=====================================
- Don't write code (write minimum code)
- If you have to write Code it should look like English
- Normally the number of lines of code should be comparable with the instructions which are issues 
  or the conditions which need to be met
- Plan for 15 min, output every 15 min.
- Code should be working all the time
- Main logic of Function should be around one screen height.
- Break things into hierarchy (into directories, subdirectories, files, functions)
- Preferably write in top down approach, sometimes leaf code can be written and tested too.
- Until you reach the leaf function, the code should be in English
- If you are not optimizing, leaf function should be in English
- Optimization for performance can make code look unreadable, 
  even in this case, keep a clean copy of parallel functionality
- Normally, for the work we do, we don't need optimization.

===================================================================== version 1
    # Override create function
    async def Create(self):
        inv_id = await super().Create()
        self.add_inventory_details(inv_id)
    
    async def Update(self):
        inv_id = super().Update()
        # Delete old formulas
        self.gdb.query(InventoryFormula).filter(InventoryFormula.inventory_id == inv_id).delete()
        # Delete old labels
        self.gdb.query(InventoryLabel).filter(InventoryLabel.inventory_id == inv_id).delete()
        # Update inventory leaf tables
        self.add_inventory_details(inv_id)
    
    def add_inventory_details(self, inv_id):
        # fetch formulas list from post data
        formulas_list = self.Data['formulas']
        for formula_id in formulas_list:
            new_inv_form = InventoryFormula()
            new_inv_form.formula_id = int(formula_id)
            new_inv_form.inv_id = inv_id
            self.gdb.add(new_inv_form)
        # fetch labels list from post data
        labels_list = self.Data['labels']
        for label_id in labels_list:
            new_inv_label = InventoryLabel()
            new_inv_label.label_id = int(label_id)
            new_inv_label.inv_id = inv_id
            self.gdb.add(new_inv_label)
    
    async def createTester(self):
        print("Test Print Create")
        self.Data['created_by_id'] = 1
        print(self.Data)
        await self.Create()
        return {}

================================================= version 2
class InventoryController(CGBaseController):
    def prepare(self):
        self.Model = Inventory
        self.Slug = 'inventories'
        self.PublicMethods = ['createTester']
        super().prepare()

    # Override create function
    async def CreateMany2Many(self):
        inv_id = await super().Create()
        await self.add_inventory_details(inv_id['data'])

    async def Update(self):
        inv_id = super().Update()

        # Delete old formulas and Tables. Also add entries in leaf tables.
        self.gdb.query(InventoryFormula).filter(InventoryFormula.inventory_id == inv_id).delete()
        self.gdb.query(InventoryLabel).filter(InventoryLabel.inventory_id == inv_id).delete()
        await self.add_inventory_details(inv_id)

    async def add_inventory_details(self, inv_id):

        # fetch formulas list from post data
        if 'formulas' in self.Data:
            formulas_list = self.Data['formulas']

            for formula_id in formulas_list:
                rec = {
                    'formula_id': int(formula_id),
                    'inventory_id': inv_id,
                }
                new_inv_form_id = await InventoryFormulaController().Create(rec)

        # fetch labels list from post data
        if 'labels' in self.Data:
            labels_list = self.Data['labels']

            for label_id in labels_list:
                rec = {
                    'label_id': int(label_id),
                    'inventory_id': inv_id,
                }
                inv_label_obj = super()._loadController("inventory_labels", data=rec)
                new_inv_label_id = await inv_label_obj.Create()

    async def createTester(self):
        await self.Create()
        return {}














